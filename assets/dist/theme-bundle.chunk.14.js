(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[14],{

/***/ "./assets/js/theme/category.js":
/*!*************************************!*\
  !*** ./assets/js/theme/category.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Category; });
/* harmony import */ var _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @bigcommerce/stencil-utils */ "./node_modules/@bigcommerce/stencil-utils/src/main.js");
/* harmony import */ var _catalog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./catalog */ "./assets/js/theme/catalog.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.min.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _global_compare_products__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./global/compare-products */ "./assets/js/theme/global/compare-products.js");
/* harmony import */ var _common_faceted_search__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./common/faceted-search */ "./assets/js/theme/common/faceted-search.js");
function _inheritsLoose(subClass, superClass) { subClass.prototype = Object.create(superClass.prototype); subClass.prototype.constructor = subClass; subClass.__proto__ = superClass; }







var Category = /*#__PURE__*/function (_CatalogPage) {
  _inheritsLoose(Category, _CatalogPage);

  function Category() {
    return _CatalogPage.apply(this, arguments) || this;
  }

  var _proto = Category.prototype;

  _proto.onReady = function onReady() {
    Object(_global_compare_products__WEBPACK_IMPORTED_MODULE_3__["default"])(this.context.urls);

    if (jquery__WEBPACK_IMPORTED_MODULE_2___default()('#facetedSearch').length > 0) {
      this.initFacetedSearch();
    } else {
      this.onSortBySubmit = this.onSortBySubmit.bind(this);
      _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_0__["hooks"].on('sortBy-submitted', this.onSortBySubmit);
    }

    this.sscorShippingHandler();
  };

  _proto.sscorShippingHandler = function sscorShippingHandler() {
    var cookies = document.cookie;
    var customerId = jquery__WEBPACK_IMPORTED_MODULE_2___default()('#customer-id').val();
    var guestCookieExists = cookies.indexOf('guestuser') > 0;

    if (!guestCookieExists || customerId == undefined && customerId == '') {
      //show prices only to logged in customers
      console.log('sscorShippingHandler - NEED GUEST CHECK');
      var price = jquery__WEBPACK_IMPORTED_MODULE_2___default()('.price-section .price').text().trim();

      if (price.startsWith('$')) {
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('<div class="" style="display:block;margin-bottom:20px;"><a href="#" class="see-price btn" title="See Price">See Pricing Info</a></div>').insertAfter('.productView-info');
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('.price-section .price.price--withoutTax').remove();
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('.form-field--increments').remove();
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('#form-action-addToCart').remove();
      } else {
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('.price-section .price.price--withoutTax').show();
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('.price-visibility .price-section').show();
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('.price-visibility .add-to-cart-button').show();
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('.form-field--increments').show();
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('#form-action-addToCart').show();
      }
    } else {
      console.log('sscorShippingHandler - CUSTOMER');
      jquery__WEBPACK_IMPORTED_MODULE_2___default()('.price-section .price.price--withoutTax').show();
      jquery__WEBPACK_IMPORTED_MODULE_2___default()('.price-visibility .price-section').show();
      jquery__WEBPACK_IMPORTED_MODULE_2___default()('.price-visibility .add-to-cart-button').show();
      jquery__WEBPACK_IMPORTED_MODULE_2___default()('.form-field--increments').show();
      jquery__WEBPACK_IMPORTED_MODULE_2___default()('#form-action-addToCart').show();
    }

    jquery__WEBPACK_IMPORTED_MODULE_2___default()('.see-price').on('click', function (evt) {
      evt.preventDefault();
      var url = document.querySelector("link[rel='canonical']").href;
      var name = jquery__WEBPACK_IMPORTED_MODULE_2___default()('.productView-title').text();
      document.cookie = "price_product=" + url + "; expires=Thu, 18 Dec 2099 12:00:00 UTC; path=/";
      document.cookie = "price_name=" + name + "; expires=Thu, 18 Dec 2099 12:00:00 UTC; path=/";
      window.location.href = '/guest-check-in/';
    });
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('embed').attr('width', '400px').attr('height', '400px');
  };

  _proto.initFacetedSearch = function initFacetedSearch() {
    var $productListingContainer = jquery__WEBPACK_IMPORTED_MODULE_2___default()('#product-listing-container');
    var $facetedSearchContainer = jquery__WEBPACK_IMPORTED_MODULE_2___default()('#faceted-search-container');
    var productsPerPage = this.context.categoryProductsPerPage;
    var requestOptions = {
      config: {
        category: {
          shop_by_price: true,
          products: {
            limit: productsPerPage
          }
        }
      },
      template: {
        productListing: 'category/product-listing',
        sidebar: 'category/sidebar'
      },
      showMore: 'category/show-more'
    };
    this.facetedSearch = new _common_faceted_search__WEBPACK_IMPORTED_MODULE_4__["default"](requestOptions, function (content) {
      $productListingContainer.html(content.productListing);
      $facetedSearchContainer.html(content.sidebar);
      jquery__WEBPACK_IMPORTED_MODULE_2___default()('body').triggerHandler('compareReset');
      jquery__WEBPACK_IMPORTED_MODULE_2___default()('html, body').animate({
        scrollTop: 0
      }, 100);
    });
  };

  return Category;
}(_catalog__WEBPACK_IMPORTED_MODULE_1__["default"]);



/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvdGhlbWUvY2F0ZWdvcnkuanMiXSwibmFtZXMiOlsiQ2F0ZWdvcnkiLCJvblJlYWR5IiwiY29tcGFyZVByb2R1Y3RzIiwiY29udGV4dCIsInVybHMiLCIkIiwibGVuZ3RoIiwiaW5pdEZhY2V0ZWRTZWFyY2giLCJvblNvcnRCeVN1Ym1pdCIsImJpbmQiLCJob29rcyIsIm9uIiwic3Njb3JTaGlwcGluZ0hhbmRsZXIiLCJjb29raWVzIiwiZG9jdW1lbnQiLCJjb29raWUiLCJjdXN0b21lcklkIiwidmFsIiwiZ3Vlc3RDb29raWVFeGlzdHMiLCJpbmRleE9mIiwidW5kZWZpbmVkIiwiY29uc29sZSIsImxvZyIsInByaWNlIiwidGV4dCIsInRyaW0iLCJzdGFydHNXaXRoIiwiaW5zZXJ0QWZ0ZXIiLCJyZW1vdmUiLCJzaG93IiwiZXZ0IiwicHJldmVudERlZmF1bHQiLCJ1cmwiLCJxdWVyeVNlbGVjdG9yIiwiaHJlZiIsIm5hbWUiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsImF0dHIiLCIkcHJvZHVjdExpc3RpbmdDb250YWluZXIiLCIkZmFjZXRlZFNlYXJjaENvbnRhaW5lciIsInByb2R1Y3RzUGVyUGFnZSIsImNhdGVnb3J5UHJvZHVjdHNQZXJQYWdlIiwicmVxdWVzdE9wdGlvbnMiLCJjb25maWciLCJjYXRlZ29yeSIsInNob3BfYnlfcHJpY2UiLCJwcm9kdWN0cyIsImxpbWl0IiwidGVtcGxhdGUiLCJwcm9kdWN0TGlzdGluZyIsInNpZGViYXIiLCJzaG93TW9yZSIsImZhY2V0ZWRTZWFyY2giLCJGYWNldGVkU2VhcmNoIiwiY29udGVudCIsImh0bWwiLCJ0cmlnZ2VySGFuZGxlciIsImFuaW1hdGUiLCJzY3JvbGxUb3AiLCJDYXRhbG9nUGFnZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztJQUVxQkEsUTs7Ozs7Ozs7O1NBQ2pCQyxPLEdBQUEsbUJBQVU7QUFDTkMsNEVBQWUsQ0FBQyxLQUFLQyxPQUFMLENBQWFDLElBQWQsQ0FBZjs7QUFFQSxRQUFJQyw2Q0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0JDLE1BQXBCLEdBQTZCLENBQWpDLEVBQW9DO0FBQ2hDLFdBQUtDLGlCQUFMO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsV0FBS0MsY0FBTCxHQUFzQixLQUFLQSxjQUFMLENBQW9CQyxJQUFwQixDQUF5QixJQUF6QixDQUF0QjtBQUNBQyxzRUFBSyxDQUFDQyxFQUFOLENBQVMsa0JBQVQsRUFBNkIsS0FBS0gsY0FBbEM7QUFDSDs7QUFFRCxTQUFLSSxvQkFBTDtBQUNILEc7O1NBRURBLG9CLEdBQUEsZ0NBQXVCO0FBRW5CLFFBQUlDLE9BQU8sR0FBR0MsUUFBUSxDQUFDQyxNQUF2QjtBQUNBLFFBQUlDLFVBQVUsR0FBR1gsNkNBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JZLEdBQWxCLEVBQWpCO0FBQ0EsUUFBSUMsaUJBQWlCLEdBQUdMLE9BQU8sQ0FBQ00sT0FBUixDQUFnQixXQUFoQixJQUErQixDQUF2RDs7QUFFQSxRQUFJLENBQUNELGlCQUFELElBQXVCRixVQUFVLElBQUlJLFNBQWQsSUFBMkJKLFVBQVUsSUFBSSxFQUFwRSxFQUF5RTtBQUNyRTtBQUNDSyxhQUFPLENBQUNDLEdBQVIsQ0FBWSx5Q0FBWjtBQUNELFVBQUlDLEtBQUssR0FBSWxCLDZDQUFDLENBQUMsdUJBQUQsQ0FBRCxDQUEyQm1CLElBQTNCLEdBQWtDQyxJQUFsQyxFQUFiOztBQUNBLFVBQUlGLEtBQUssQ0FBQ0csVUFBTixDQUFpQixHQUFqQixDQUFKLEVBQTJCO0FBQ3ZCckIscURBQUMsQ0FBQyx3SUFBRCxDQUFELENBQTRJc0IsV0FBNUksQ0FBd0osbUJBQXhKO0FBQ0F0QixxREFBQyxDQUFDLHlDQUFELENBQUQsQ0FBNkN1QixNQUE3QztBQUNBdkIscURBQUMsQ0FBQyx5QkFBRCxDQUFELENBQTZCdUIsTUFBN0I7QUFDQXZCLHFEQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QnVCLE1BQTVCO0FBQ0gsT0FMRCxNQUtPO0FBQ0h2QixxREFBQyxDQUFDLHlDQUFELENBQUQsQ0FBNkN3QixJQUE3QztBQUNBeEIscURBQUMsQ0FBQyxrQ0FBRCxDQUFELENBQXNDd0IsSUFBdEM7QUFDQXhCLHFEQUFDLENBQUMsdUNBQUQsQ0FBRCxDQUEyQ3dCLElBQTNDO0FBQ0F4QixxREFBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJ3QixJQUE3QjtBQUNBeEIscURBQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCd0IsSUFBNUI7QUFHSDtBQUVKLEtBbkJELE1BbUJPO0FBQ0hSLGFBQU8sQ0FBQ0MsR0FBUixDQUFZLGlDQUFaO0FBQ0FqQixtREFBQyxDQUFDLHlDQUFELENBQUQsQ0FBNkN3QixJQUE3QztBQUNBeEIsbURBQUMsQ0FBQyxrQ0FBRCxDQUFELENBQXNDd0IsSUFBdEM7QUFDQXhCLG1EQUFDLENBQUMsdUNBQUQsQ0FBRCxDQUEyQ3dCLElBQTNDO0FBQ0F4QixtREFBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJ3QixJQUE3QjtBQUNBeEIsbURBQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCd0IsSUFBNUI7QUFDSDs7QUFHRHhCLGlEQUFDLENBQUMsWUFBRCxDQUFELENBQWdCTSxFQUFoQixDQUFtQixPQUFuQixFQUE0QixVQUFTbUIsR0FBVCxFQUFhO0FBQ3JDQSxTQUFHLENBQUNDLGNBQUo7QUFDQSxVQUFJQyxHQUFHLEdBQUdsQixRQUFRLENBQUNtQixhQUFULENBQXVCLHVCQUF2QixFQUFnREMsSUFBMUQ7QUFDQSxVQUFJQyxJQUFJLEdBQUc5Qiw2Q0FBQyxDQUFDLG9CQUFELENBQUQsQ0FBd0JtQixJQUF4QixFQUFYO0FBQ0FWLGNBQVEsQ0FBQ0MsTUFBVCxHQUFrQixtQkFBa0JpQixHQUFsQixHQUF1QixpREFBekM7QUFDQWxCLGNBQVEsQ0FBQ0MsTUFBVCxHQUFrQixnQkFBZW9CLElBQWYsR0FBcUIsaURBQXZDO0FBQ0FDLFlBQU0sQ0FBQ0MsUUFBUCxDQUFnQkgsSUFBaEIsR0FBd0Isa0JBQXhCO0FBQ0gsS0FQRDtBQVNBN0IsaURBQUMsQ0FBQyxPQUFELENBQUQsQ0FBV2lDLElBQVgsQ0FBZ0IsT0FBaEIsRUFBeUIsT0FBekIsRUFBa0NBLElBQWxDLENBQXVDLFFBQXZDLEVBQWdELE9BQWhEO0FBR0gsRzs7U0FFRC9CLGlCLEdBQUEsNkJBQW9CO0FBQ2hCLFFBQU1nQyx3QkFBd0IsR0FBR2xDLDZDQUFDLENBQUMsNEJBQUQsQ0FBbEM7QUFDQSxRQUFNbUMsdUJBQXVCLEdBQUduQyw2Q0FBQyxDQUFDLDJCQUFELENBQWpDO0FBQ0EsUUFBTW9DLGVBQWUsR0FBRyxLQUFLdEMsT0FBTCxDQUFhdUMsdUJBQXJDO0FBQ0EsUUFBTUMsY0FBYyxHQUFHO0FBQ25CQyxZQUFNLEVBQUU7QUFDSkMsZ0JBQVEsRUFBRTtBQUNOQyx1QkFBYSxFQUFFLElBRFQ7QUFFTkMsa0JBQVEsRUFBRTtBQUNOQyxpQkFBSyxFQUFFUDtBQUREO0FBRko7QUFETixPQURXO0FBU25CUSxjQUFRLEVBQUU7QUFDTkMsc0JBQWMsRUFBRSwwQkFEVjtBQUVOQyxlQUFPLEVBQUU7QUFGSCxPQVRTO0FBYW5CQyxjQUFRLEVBQUU7QUFiUyxLQUF2QjtBQWdCQSxTQUFLQyxhQUFMLEdBQXFCLElBQUlDLDhEQUFKLENBQWtCWCxjQUFsQixFQUFrQyxVQUFDWSxPQUFELEVBQWE7QUFDaEVoQiw4QkFBd0IsQ0FBQ2lCLElBQXpCLENBQThCRCxPQUFPLENBQUNMLGNBQXRDO0FBQ0FWLDZCQUF1QixDQUFDZ0IsSUFBeEIsQ0FBNkJELE9BQU8sQ0FBQ0osT0FBckM7QUFFQTlDLG1EQUFDLENBQUMsTUFBRCxDQUFELENBQVVvRCxjQUFWLENBQXlCLGNBQXpCO0FBRUFwRCxtREFBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQnFELE9BQWhCLENBQXdCO0FBQ3BCQyxpQkFBUyxFQUFFO0FBRFMsT0FBeEIsRUFFRyxHQUZIO0FBR0gsS0FUb0IsQ0FBckI7QUFVSCxHOzs7RUE3RmlDQyxnRCIsImZpbGUiOiJ0aGVtZS1idW5kbGUuY2h1bmsuMTQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBob29rcyB9IGZyb20gJ0BiaWdjb21tZXJjZS9zdGVuY2lsLXV0aWxzJztcbmltcG9ydCBDYXRhbG9nUGFnZSBmcm9tICcuL2NhdGFsb2cnO1xuaW1wb3J0ICQgZnJvbSAnanF1ZXJ5JztcbmltcG9ydCBjb21wYXJlUHJvZHVjdHMgZnJvbSAnLi9nbG9iYWwvY29tcGFyZS1wcm9kdWN0cyc7XG5pbXBvcnQgRmFjZXRlZFNlYXJjaCBmcm9tICcuL2NvbW1vbi9mYWNldGVkLXNlYXJjaCc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENhdGVnb3J5IGV4dGVuZHMgQ2F0YWxvZ1BhZ2Uge1xuICAgIG9uUmVhZHkoKSB7XG4gICAgICAgIGNvbXBhcmVQcm9kdWN0cyh0aGlzLmNvbnRleHQudXJscyk7XG5cbiAgICAgICAgaWYgKCQoJyNmYWNldGVkU2VhcmNoJykubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgdGhpcy5pbml0RmFjZXRlZFNlYXJjaCgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5vblNvcnRCeVN1Ym1pdCA9IHRoaXMub25Tb3J0QnlTdWJtaXQuYmluZCh0aGlzKTtcbiAgICAgICAgICAgIGhvb2tzLm9uKCdzb3J0Qnktc3VibWl0dGVkJywgdGhpcy5vblNvcnRCeVN1Ym1pdCk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnNzY29yU2hpcHBpbmdIYW5kbGVyKCk7XG4gICAgfVxuXG4gICAgc3Njb3JTaGlwcGluZ0hhbmRsZXIoKSB7XG5cbiAgICAgICAgdmFyIGNvb2tpZXMgPSBkb2N1bWVudC5jb29raWU7XG4gICAgICAgIHZhciBjdXN0b21lcklkID0gJCgnI2N1c3RvbWVyLWlkJykudmFsKCk7XG4gICAgICAgIHZhciBndWVzdENvb2tpZUV4aXN0cyA9IGNvb2tpZXMuaW5kZXhPZignZ3Vlc3R1c2VyJykgPiAwO1xuXG4gICAgICAgIGlmICghZ3Vlc3RDb29raWVFeGlzdHMgfHwgKGN1c3RvbWVySWQgPT0gdW5kZWZpbmVkICYmIGN1c3RvbWVySWQgPT0gJycpKSB7XG4gICAgICAgICAgICAvL3Nob3cgcHJpY2VzIG9ubHkgdG8gbG9nZ2VkIGluIGN1c3RvbWVyc1xuICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdzc2NvclNoaXBwaW5nSGFuZGxlciAtIE5FRUQgR1VFU1QgQ0hFQ0snKTtcbiAgICAgICAgICAgIHZhciBwcmljZSA9ICAkKCcucHJpY2Utc2VjdGlvbiAucHJpY2UnKS50ZXh0KCkudHJpbSgpO1xuICAgICAgICAgICAgaWYgKHByaWNlLnN0YXJ0c1dpdGgoJyQnKSkge1xuICAgICAgICAgICAgICAgICQoJzxkaXYgY2xhc3M9XCJcIiBzdHlsZT1cImRpc3BsYXk6YmxvY2s7bWFyZ2luLWJvdHRvbToyMHB4O1wiPjxhIGhyZWY9XCIjXCIgY2xhc3M9XCJzZWUtcHJpY2UgYnRuXCIgdGl0bGU9XCJTZWUgUHJpY2VcIj5TZWUgUHJpY2luZyBJbmZvPC9hPjwvZGl2PicpLmluc2VydEFmdGVyKCcucHJvZHVjdFZpZXctaW5mbycpO1xuICAgICAgICAgICAgICAgICQoJy5wcmljZS1zZWN0aW9uIC5wcmljZS5wcmljZS0td2l0aG91dFRheCcpLnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgICQoJy5mb3JtLWZpZWxkLS1pbmNyZW1lbnRzJykucmVtb3ZlKCk7XG4gICAgICAgICAgICAgICAgJCgnI2Zvcm0tYWN0aW9uLWFkZFRvQ2FydCcpLnJlbW92ZSgpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkKCcucHJpY2Utc2VjdGlvbiAucHJpY2UucHJpY2UtLXdpdGhvdXRUYXgnKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgJCgnLnByaWNlLXZpc2liaWxpdHkgLnByaWNlLXNlY3Rpb24nKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgJCgnLnByaWNlLXZpc2liaWxpdHkgLmFkZC10by1jYXJ0LWJ1dHRvbicpLnNob3coKTtcbiAgICAgICAgICAgICAgICAkKCcuZm9ybS1maWVsZC0taW5jcmVtZW50cycpLnNob3coKTtcbiAgICAgICAgICAgICAgICAkKCcjZm9ybS1hY3Rpb24tYWRkVG9DYXJ0Jykuc2hvdygpO1xuXG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ3NzY29yU2hpcHBpbmdIYW5kbGVyIC0gQ1VTVE9NRVInKTtcbiAgICAgICAgICAgICQoJy5wcmljZS1zZWN0aW9uIC5wcmljZS5wcmljZS0td2l0aG91dFRheCcpLnNob3coKTtcbiAgICAgICAgICAgICQoJy5wcmljZS12aXNpYmlsaXR5IC5wcmljZS1zZWN0aW9uJykuc2hvdygpO1xuICAgICAgICAgICAgJCgnLnByaWNlLXZpc2liaWxpdHkgLmFkZC10by1jYXJ0LWJ1dHRvbicpLnNob3coKTtcbiAgICAgICAgICAgICQoJy5mb3JtLWZpZWxkLS1pbmNyZW1lbnRzJykuc2hvdygpO1xuICAgICAgICAgICAgJCgnI2Zvcm0tYWN0aW9uLWFkZFRvQ2FydCcpLnNob3coKTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgJCgnLnNlZS1wcmljZScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGV2dCl7XG4gICAgICAgICAgICBldnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwibGlua1tyZWw9J2Nhbm9uaWNhbCddXCIpLmhyZWY7XG4gICAgICAgICAgICB2YXIgbmFtZSA9ICQoJy5wcm9kdWN0Vmlldy10aXRsZScpLnRleHQoKTtcbiAgICAgICAgICAgIGRvY3VtZW50LmNvb2tpZSA9IFwicHJpY2VfcHJvZHVjdD1cIisgdXJsICtcIjsgZXhwaXJlcz1UaHUsIDE4IERlYyAyMDk5IDEyOjAwOjAwIFVUQzsgcGF0aD0vXCI7XG4gICAgICAgICAgICBkb2N1bWVudC5jb29raWUgPSBcInByaWNlX25hbWU9XCIrIG5hbWUgK1wiOyBleHBpcmVzPVRodSwgMTggRGVjIDIwOTkgMTI6MDA6MDAgVVRDOyBwYXRoPS9cIjtcbiAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gKCcvZ3Vlc3QtY2hlY2staW4vJyk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICQoJ2VtYmVkJykuYXR0cignd2lkdGgnLCAnNDAwcHgnKS5hdHRyKCdoZWlnaHQnLCc0MDBweCcpO1xuXG5cbiAgICB9XG5cbiAgICBpbml0RmFjZXRlZFNlYXJjaCgpIHtcbiAgICAgICAgY29uc3QgJHByb2R1Y3RMaXN0aW5nQ29udGFpbmVyID0gJCgnI3Byb2R1Y3QtbGlzdGluZy1jb250YWluZXInKTtcbiAgICAgICAgY29uc3QgJGZhY2V0ZWRTZWFyY2hDb250YWluZXIgPSAkKCcjZmFjZXRlZC1zZWFyY2gtY29udGFpbmVyJyk7XG4gICAgICAgIGNvbnN0IHByb2R1Y3RzUGVyUGFnZSA9IHRoaXMuY29udGV4dC5jYXRlZ29yeVByb2R1Y3RzUGVyUGFnZTtcbiAgICAgICAgY29uc3QgcmVxdWVzdE9wdGlvbnMgPSB7XG4gICAgICAgICAgICBjb25maWc6IHtcbiAgICAgICAgICAgICAgICBjYXRlZ29yeToge1xuICAgICAgICAgICAgICAgICAgICBzaG9wX2J5X3ByaWNlOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBwcm9kdWN0czoge1xuICAgICAgICAgICAgICAgICAgICAgICAgbGltaXQ6IHByb2R1Y3RzUGVyUGFnZSxcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRlbXBsYXRlOiB7XG4gICAgICAgICAgICAgICAgcHJvZHVjdExpc3Rpbmc6ICdjYXRlZ29yeS9wcm9kdWN0LWxpc3RpbmcnLFxuICAgICAgICAgICAgICAgIHNpZGViYXI6ICdjYXRlZ29yeS9zaWRlYmFyJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBzaG93TW9yZTogJ2NhdGVnb3J5L3Nob3ctbW9yZScsXG4gICAgICAgIH07XG5cbiAgICAgICAgdGhpcy5mYWNldGVkU2VhcmNoID0gbmV3IEZhY2V0ZWRTZWFyY2gocmVxdWVzdE9wdGlvbnMsIChjb250ZW50KSA9PiB7XG4gICAgICAgICAgICAkcHJvZHVjdExpc3RpbmdDb250YWluZXIuaHRtbChjb250ZW50LnByb2R1Y3RMaXN0aW5nKTtcbiAgICAgICAgICAgICRmYWNldGVkU2VhcmNoQ29udGFpbmVyLmh0bWwoY29udGVudC5zaWRlYmFyKTtcblxuICAgICAgICAgICAgJCgnYm9keScpLnRyaWdnZXJIYW5kbGVyKCdjb21wYXJlUmVzZXQnKTtcblxuICAgICAgICAgICAgJCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgIHNjcm9sbFRvcDogMCxcbiAgICAgICAgICAgIH0sIDEwMCk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbiJdLCJzb3VyY2VSb290IjoiIn0=