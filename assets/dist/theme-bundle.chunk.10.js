(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./assets/js/theme/cart.js":
/*!*********************************!*\
  !*** ./assets/js/theme/cart.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Cart; });
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash_debounce__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash_bind__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash/bind */ "./node_modules/lodash/bind.js");
/* harmony import */ var lodash_bind__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_bind__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _page_manager__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./page-manager */ "./assets/js/theme/page-manager.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.min.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _common_gift_certificate_validator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./common/gift-certificate-validator */ "./assets/js/theme/common/gift-certificate-validator.js");
/* harmony import */ var _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @bigcommerce/stencil-utils */ "./node_modules/@bigcommerce/stencil-utils/src/main.js");
/* harmony import */ var _cart_shipping_estimator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cart/shipping-estimator */ "./assets/js/theme/cart/shipping-estimator.js");
/* harmony import */ var _global_modal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./global/modal */ "./assets/js/theme/global/modal.js");
/* harmony import */ var _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./global/sweet-alert */ "./assets/js/theme/global/sweet-alert.js");



function _inheritsLoose(subClass, superClass) { subClass.prototype = Object.create(superClass.prototype); subClass.prototype.constructor = subClass; subClass.__proto__ = superClass; }









function setShippingMessage(inv, quantity, isDefault) {
  var leadtime = 5;
  var trigger = 25;
  var nbdMsg = " Scheduled to ship next business day";
  var leadtimeMsg = " Scheduled to ship in " + (1 + leadtime) + " business days";
  var available = inv;
  var qty = quantity;
  var diff = available - qty;
  var message = '';

  if (isDefault) {
    //quantity is 1 by default
    if (available > 0 && quantity <= available) {
      message = 'Ships Next Business Day';
    } else if (available <= 0 && diff > -25) {
      message = 'Ships in ' + (1 + leadtime) + ' business days';
    } else if (available <= 0 && diff <= -25) {
      message = 'Please contact us for a ship date';
    }
  } else {
    if (diff >= 0) {
      if (diff < available && available > 0) {
        message = qty + nbdMsg;
      }
    } else {
      if (diff < trigger && available > 0) {
        if (diff <= -trigger) {
          message = 'Please contact us for a ship date';
        } else {
          message = available + nbdMsg + "; " + -diff + leadtimeMsg;
        }
      } else if (-diff < trigger && available <= 0) {
        message = leadtimeMsg;
      } else {
        message = 'Please contact us for a ship date';
      }
    }
  } //console.log( "  Delta is:  " + diff + " , Qty Requested: " + quantity +" , Qty On Hand: " + available);


  return message;
}

var Cart = /*#__PURE__*/function (_PageManager) {
  _inheritsLoose(Cart, _PageManager);

  function Cart() {
    return _PageManager.apply(this, arguments) || this;
  }

  var _proto = Cart.prototype;

  _proto.onReady = function onReady() {
    this.$cartContent = jquery__WEBPACK_IMPORTED_MODULE_3___default()('[data-cart-content]');
    this.$cartMessages = jquery__WEBPACK_IMPORTED_MODULE_3___default()('[data-cart-status]');
    this.$cartTotals = jquery__WEBPACK_IMPORTED_MODULE_3___default()('[data-cart-totals]');
    this.$overlay = jquery__WEBPACK_IMPORTED_MODULE_3___default()('[data-cart] .loadingOverlay').hide(); // TODO: temporary until roper pulls in his cart components

    this.bindEvents();
    jquery__WEBPACK_IMPORTED_MODULE_3___default()('.cart-item-qty-input').each(function () {
      var itemId = jquery__WEBPACK_IMPORTED_MODULE_3___default()(this).data('cartItemid');
      var qty = jquery__WEBPACK_IMPORTED_MODULE_3___default()("#qty-" + itemId).val();
      var inv = parseInt(jquery__WEBPACK_IMPORTED_MODULE_3___default()("#sscor-inventory-" + itemId).text());
      var msg = setShippingMessage(inv, qty, false);
      console.log(msg + " on load");
      jquery__WEBPACK_IMPORTED_MODULE_3___default()("#sscor-availability-" + itemId).text(msg);
      jquery__WEBPACK_IMPORTED_MODULE_3___default()("#sscor-availability-" + itemId).show();
    });
  };

  _proto.cartUpdate = function cartUpdate($target) {
    var _this = this;

    var itemId = $target.data('cartItemid');
    var $el = jquery__WEBPACK_IMPORTED_MODULE_3___default()("#qty-" + itemId);
    var oldQty = parseInt($el.val(), 10);
    var maxQty = parseInt($el.data('quantityMax'), 10);
    var minQty = parseInt($el.data('quantityMin'), 10);
    var minError = $el.data('quantityMinError');
    var maxError = $el.data('quantityMaxError');
    var newQty = $target.data('action') === 'inc' ? oldQty + 1 : oldQty - 1; // Does not quality for min/max quantity

    if (newQty < minQty) {
      return _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
        text: minError,
        icon: 'error'
      });
    } else if (maxQty > 0 && newQty > maxQty) {
      return _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
        text: maxError,
        icon: 'error'
      });
    }

    this.$overlay.show();
    _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_5__["default"].api.cart.itemUpdate(itemId, newQty, function (err, response) {
      _this.$overlay.hide();

      if (response.data.status === 'succeed') {
        // if the quantity is changed "1" from "0", we have to remove the row.
        var remove = newQty === 0;

        _this.refreshContent(remove);
      } else {
        $el.val(oldQty);
        _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
          text: response.data.errors.join('\n'),
          icon: 'error'
        });
      }
    });
  };

  _proto.cartUpdateQtyTextChange = function cartUpdateQtyTextChange($target, preVal) {
    var _this2 = this;

    if (preVal === void 0) {
      preVal = null;
    }

    var itemId = $target.data('cartItemid');
    var $el = jquery__WEBPACK_IMPORTED_MODULE_3___default()("#qty-" + itemId);
    var maxQty = parseInt($el.data('quantityMax'), 10);
    var minQty = parseInt($el.data('quantityMin'), 10);
    var oldQty = preVal !== null ? preVal : minQty;
    var minError = $el.data('quantityMinError');
    var maxError = $el.data('quantityMaxError');
    var newQty = parseInt(Number($el.val()), 10);
    var invalidEntry; // Does not quality for min/max quantity

    if (!newQty) {
      invalidEntry = $el.val();
      $el.val(oldQty);
      return _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
        text: invalidEntry + " is not a valid entry",
        icon: 'error'
      });
    } else if (newQty < minQty) {
      $el.val(oldQty);
      return _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
        text: minError,
        icon: 'error'
      });
    } else if (maxQty > 0 && newQty > maxQty) {
      $el.val(oldQty);
      return _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
        text: maxError,
        icon: 'error'
      });
    }

    this.$overlay.show();
    _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_5__["default"].api.cart.itemUpdate(itemId, newQty, function (err, response) {
      _this2.$overlay.hide();

      if (response.data.status === 'succeed') {
        // if the quantity is changed "1" from "0", we have to remove the row.
        var remove = newQty === 0;

        _this2.refreshContent(remove);
      } else {
        $el.val(oldQty);
        _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
          text: response.data.errors.join('\n'),
          icon: 'error'
        });
      }
    });
  };

  _proto.cartRemoveItem = function cartRemoveItem(itemId) {
    var _this3 = this;

    this.$overlay.show();
    _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_5__["default"].api.cart.itemRemove(itemId, function (err, response) {
      if (response.data.status === 'succeed') {
        _this3.refreshContent(true);
      } else {
        _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
          text: response.data.errors.join('\n'),
          icon: 'error'
        });
      }
    });
  };

  _proto.cartEditOptions = function cartEditOptions(itemId) {
    var _this4 = this;

    var modal = Object(_global_modal__WEBPACK_IMPORTED_MODULE_7__["defaultModal"])();
    var options = {
      template: 'cart/modals/configure-product'
    };
    modal.open();
    _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_5__["default"].api.productAttributes.configureInCart(itemId, options, function (err, response) {
      modal.updateContent(response.content);

      _this4.bindGiftWrappingForm();
    });
    _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_5__["default"].hooks.on('product-option-change', function (event, option) {
      var $changedOption = jquery__WEBPACK_IMPORTED_MODULE_3___default()(option);
      var $form = $changedOption.parents('form');
      var $submit = jquery__WEBPACK_IMPORTED_MODULE_3___default()('input.button', $form);
      var $messageBox = jquery__WEBPACK_IMPORTED_MODULE_3___default()('.alertMessageBox');
      var item = jquery__WEBPACK_IMPORTED_MODULE_3___default()('[name="item_id"]', $form).attr('value');
      _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_5__["default"].api.productAttributes.optionChange(item, $form.serialize(), function (err, result) {
        var data = result.data || {};

        if (err) {
          _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
            text: err,
            icon: 'error'
          });
          return false;
        }

        if (data.purchasing_message) {
          jquery__WEBPACK_IMPORTED_MODULE_3___default()('p.alertBox-message', $messageBox).text(data.purchasing_message);
          $submit.prop('disabled', true);
          $messageBox.show();
        } else {
          $submit.prop('disabled', false);
          $messageBox.hide();
        }

        if (!data.purchasable || !data.instock) {
          $submit.prop('disabled', true);
        } else {
          $submit.prop('disabled', false);
        }
      });
    });
  };

  _proto.refreshContent = function refreshContent(remove) {
    var _this5 = this;

    var $cartItemsRows = jquery__WEBPACK_IMPORTED_MODULE_3___default()('[data-item-row]', this.$cartContent);
    var $cartPageTitle = jquery__WEBPACK_IMPORTED_MODULE_3___default()('[data-cart-page-title]');
    var options = {
      template: {
        content: 'cart/content',
        totals: 'cart/totals',
        pageTitle: 'cart/page-title',
        statusMessages: 'cart/status-messages'
      }
    };
    this.$overlay.show(); // Remove last item from cart? Reload

    if (remove && $cartItemsRows.length === 1) {
      return window.location.reload();
    }

    _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_5__["default"].api.cart.getContent(options, function (err, response) {
      _this5.$cartContent.html(response.content);

      _this5.$cartTotals.html(response.totals);

      _this5.$cartMessages.html(response.statusMessages);

      $cartPageTitle.replaceWith(response.pageTitle);

      _this5.bindEvents();

      _this5.$overlay.hide();

      var quantity = jquery__WEBPACK_IMPORTED_MODULE_3___default()('[data-cart-quantity]', _this5.$cartContent).data('cartQuantity') || 0;
      jquery__WEBPACK_IMPORTED_MODULE_3___default()('body').trigger('cart-quantity-update', quantity); //SSCOR

      jquery__WEBPACK_IMPORTED_MODULE_3___default()('.cart-item-qty-input').each(function () {
        var itemId = jquery__WEBPACK_IMPORTED_MODULE_3___default()(this).data('cartItemid');
        var inv = parseInt(jquery__WEBPACK_IMPORTED_MODULE_3___default()("#sscor-inventory-" + itemId).text());
        var qty = jquery__WEBPACK_IMPORTED_MODULE_3___default()("#qty-" + itemId).val();
        var msg = setShippingMessage(inv, qty, false); //   console.log(msg + " on change");

        jquery__WEBPACK_IMPORTED_MODULE_3___default()("#sscor-availability-" + itemId).text(msg);
        jquery__WEBPACK_IMPORTED_MODULE_3___default()("#sscor-availability-" + itemId).show();
      });
    });
  };

  _proto.bindCartEvents = function bindCartEvents() {
    var _this6 = this;

    var debounceTimeout = 400;

    var cartUpdate = lodash_bind__WEBPACK_IMPORTED_MODULE_1___default()(lodash_debounce__WEBPACK_IMPORTED_MODULE_0___default()(this.cartUpdate, debounceTimeout), this);

    var cartUpdateQtyTextChange = lodash_bind__WEBPACK_IMPORTED_MODULE_1___default()(lodash_debounce__WEBPACK_IMPORTED_MODULE_0___default()(this.cartUpdateQtyTextChange, debounceTimeout), this);

    var cartRemoveItem = lodash_bind__WEBPACK_IMPORTED_MODULE_1___default()(lodash_debounce__WEBPACK_IMPORTED_MODULE_0___default()(this.cartRemoveItem, debounceTimeout), this);

    var preVal; // cart update

    jquery__WEBPACK_IMPORTED_MODULE_3___default()('[data-cart-update]', this.$cartContent).on('click', function (event) {
      var $target = jquery__WEBPACK_IMPORTED_MODULE_3___default()(event.currentTarget);
      event.preventDefault(); // update cart quantity

      cartUpdate($target);
    }); // cart qty manually updates

    jquery__WEBPACK_IMPORTED_MODULE_3___default()('.cart-item-qty-input', this.$cartContent).on('focus', function onQtyFocus() {
      preVal = this.value;
    }).change(function (event) {
      var $target = jquery__WEBPACK_IMPORTED_MODULE_3___default()(event.currentTarget);
      event.preventDefault(); // update cart quantity

      cartUpdateQtyTextChange($target, preVal);
    });
    jquery__WEBPACK_IMPORTED_MODULE_3___default()('.cart-remove', this.$cartContent).on('click', function (event) {
      var itemId = jquery__WEBPACK_IMPORTED_MODULE_3___default()(event.currentTarget).data('cartItemid');
      var string = jquery__WEBPACK_IMPORTED_MODULE_3___default()(event.currentTarget).data('confirmDelete');
      _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
        text: string,
        icon: 'warning',
        showCancelButton: true
      }).then(function (result) {
        if (result.value) {
          // remove item from cart
          cartRemoveItem(itemId);
        }
      });
      event.preventDefault();
    });
    jquery__WEBPACK_IMPORTED_MODULE_3___default()('[data-item-edit]', this.$cartContent).on('click', function (event) {
      var itemId = jquery__WEBPACK_IMPORTED_MODULE_3___default()(event.currentTarget).data('itemEdit');
      event.preventDefault(); // edit item in cart

      _this6.cartEditOptions(itemId);
    });
  };

  _proto.bindPromoCodeEvents = function bindPromoCodeEvents() {
    var _this7 = this;

    var $couponContainer = jquery__WEBPACK_IMPORTED_MODULE_3___default()('.coupon-code');
    var $couponForm = jquery__WEBPACK_IMPORTED_MODULE_3___default()('.coupon-form');
    var $codeInput = jquery__WEBPACK_IMPORTED_MODULE_3___default()('[name="couponcode"]', $couponForm);
    jquery__WEBPACK_IMPORTED_MODULE_3___default()('.coupon-code-add').on('click', function (event) {
      event.preventDefault();
      jquery__WEBPACK_IMPORTED_MODULE_3___default()(event.currentTarget).hide();
      $couponContainer.show();
      jquery__WEBPACK_IMPORTED_MODULE_3___default()('.coupon-code-cancel').show();
      $codeInput.trigger('focus');
    });
    jquery__WEBPACK_IMPORTED_MODULE_3___default()('.coupon-code-cancel').on('click', function (event) {
      event.preventDefault();
      $couponContainer.hide();
      jquery__WEBPACK_IMPORTED_MODULE_3___default()('.coupon-code-cancel').hide();
      jquery__WEBPACK_IMPORTED_MODULE_3___default()('.coupon-code-add').show();
    });
    $couponForm.on('submit', function (event) {
      var code = $codeInput.val();
      event.preventDefault(); // Empty code

      if (!code) {
        return _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
          text: $codeInput.data('error'),
          icon: 'error'
        });
      }

      _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_5__["default"].api.cart.applyCode(code, function (err, response) {
        if (response.data.status === 'success') {
          _this7.refreshContent();
        } else {
          _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
            text: response.data.errors.join('\n'),
            icon: 'error'
          });
        }
      });
    });
  };

  _proto.bindGiftCertificateEvents = function bindGiftCertificateEvents() {
    var _this8 = this;

    var $certContainer = jquery__WEBPACK_IMPORTED_MODULE_3___default()('.gift-certificate-code');
    var $certForm = jquery__WEBPACK_IMPORTED_MODULE_3___default()('.cart-gift-certificate-form');
    var $certInput = jquery__WEBPACK_IMPORTED_MODULE_3___default()('[name="certcode"]', $certForm);
    jquery__WEBPACK_IMPORTED_MODULE_3___default()('.gift-certificate-add').on('click', function (event) {
      event.preventDefault();
      jquery__WEBPACK_IMPORTED_MODULE_3___default()(event.currentTarget).toggle();
      $certContainer.toggle();
      jquery__WEBPACK_IMPORTED_MODULE_3___default()('.gift-certificate-cancel').toggle();
    });
    jquery__WEBPACK_IMPORTED_MODULE_3___default()('.gift-certificate-cancel').on('click', function (event) {
      event.preventDefault();
      $certContainer.toggle();
      jquery__WEBPACK_IMPORTED_MODULE_3___default()('.gift-certificate-add').toggle();
      jquery__WEBPACK_IMPORTED_MODULE_3___default()('.gift-certificate-cancel').toggle();
    });
    $certForm.on('submit', function (event) {
      var code = $certInput.val();
      event.preventDefault();

      if (!Object(_common_gift_certificate_validator__WEBPACK_IMPORTED_MODULE_4__["default"])(code)) {
        return _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
          text: $certInput.data('error'),
          icon: 'error'
        });
      }

      _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_5__["default"].api.cart.applyGiftCertificate(code, function (err, resp) {
        if (resp.data.status === 'success') {
          _this8.refreshContent();
        } else {
          _global_sweet_alert__WEBPACK_IMPORTED_MODULE_8__["default"].fire({
            text: resp.data.errors.join('\n'),
            icon: 'error'
          });
        }
      });
    });
  };

  _proto.bindGiftWrappingEvents = function bindGiftWrappingEvents() {
    var _this9 = this;

    var modal = Object(_global_modal__WEBPACK_IMPORTED_MODULE_7__["defaultModal"])();
    jquery__WEBPACK_IMPORTED_MODULE_3___default()('[data-item-giftwrap]').on('click', function (event) {
      var itemId = jquery__WEBPACK_IMPORTED_MODULE_3___default()(event.currentTarget).data('itemGiftwrap');
      var options = {
        template: 'cart/modals/gift-wrapping-form'
      };
      event.preventDefault();
      modal.open();
      _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_5__["default"].api.cart.getItemGiftWrappingOptions(itemId, options, function (err, response) {
        modal.updateContent(response.content);

        _this9.bindGiftWrappingForm();
      });
    });
  };

  _proto.bindGiftWrappingForm = function bindGiftWrappingForm() {
    jquery__WEBPACK_IMPORTED_MODULE_3___default()('.giftWrapping-select').on('change', function (event) {
      var $select = jquery__WEBPACK_IMPORTED_MODULE_3___default()(event.currentTarget);
      var id = $select.val();
      var index = $select.data('index');

      if (!id) {
        return;
      }

      var allowMessage = $select.find("option[value=" + id + "]").data('allowMessage');
      jquery__WEBPACK_IMPORTED_MODULE_3___default()(".giftWrapping-image-" + index).hide();
      jquery__WEBPACK_IMPORTED_MODULE_3___default()("#giftWrapping-image-" + index + "-" + id).show();

      if (allowMessage) {
        jquery__WEBPACK_IMPORTED_MODULE_3___default()("#giftWrapping-message-" + index).show();
      } else {
        jquery__WEBPACK_IMPORTED_MODULE_3___default()("#giftWrapping-message-" + index).hide();
      }
    });
    jquery__WEBPACK_IMPORTED_MODULE_3___default()('.giftWrapping-select').trigger('change');

    function toggleViews() {
      var value = jquery__WEBPACK_IMPORTED_MODULE_3___default()('input:radio[name ="giftwraptype"]:checked').val();
      var $singleForm = jquery__WEBPACK_IMPORTED_MODULE_3___default()('.giftWrapping-single');
      var $multiForm = jquery__WEBPACK_IMPORTED_MODULE_3___default()('.giftWrapping-multiple');

      if (value === 'same') {
        $singleForm.show();
        $multiForm.hide();
      } else {
        $singleForm.hide();
        $multiForm.show();
      }
    }

    jquery__WEBPACK_IMPORTED_MODULE_3___default()('[name="giftwraptype"]').on('click', toggleViews);
    toggleViews();
  };

  _proto.bindEvents = function bindEvents() {
    this.bindCartEvents();
    this.bindPromoCodeEvents();
    this.bindGiftWrappingEvents();
    this.bindGiftCertificateEvents(); // initiate shipping estimator module

    this.shippingEstimator = new _cart_shipping_estimator__WEBPACK_IMPORTED_MODULE_6__["default"](jquery__WEBPACK_IMPORTED_MODULE_3___default()('[data-shipping-estimator]'));
  };

  return Cart;
}(_page_manager__WEBPACK_IMPORTED_MODULE_2__["default"]);



/***/ }),

/***/ "./assets/js/theme/cart/shipping-estimator.js":
/*!****************************************************!*\
  !*** ./assets/js/theme/cart/shipping-estimator.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ShippingEstimator; });
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.min.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _common_state_country__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/state-country */ "./assets/js/theme/common/state-country.js");
/* harmony import */ var _common_nod__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/nod */ "./assets/js/theme/common/nod.js");
/* harmony import */ var _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @bigcommerce/stencil-utils */ "./node_modules/@bigcommerce/stencil-utils/src/main.js");
/* harmony import */ var _common_form_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/form-utils */ "./assets/js/theme/common/form-utils.js");
/* harmony import */ var _global_sweet_alert__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../global/sweet-alert */ "./assets/js/theme/global/sweet-alert.js");







var ShippingEstimator = /*#__PURE__*/function () {
  function ShippingEstimator($element) {
    this.$element = $element;
    this.$state = jquery__WEBPACK_IMPORTED_MODULE_0___default()('[data-field-type="State"]', this.$element);
    this.initFormValidation();
    this.bindStateCountryChange();
    this.bindEstimatorEvents();
  }

  var _proto = ShippingEstimator.prototype;

  _proto.initFormValidation = function initFormValidation() {
    var _this = this;

    this.shippingEstimator = 'form[data-shipping-estimator]';
    this.shippingValidator = Object(_common_nod__WEBPACK_IMPORTED_MODULE_2__["default"])({
      submit: this.shippingEstimator + " .shipping-estimate-submit"
    });
    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.shipping-estimate-submit', this.$element).on('click', function (event) {
      // When switching between countries, the state/region is dynamic
      // Only perform a check for all fields when country has a value
      // Otherwise areAll('valid') will check country for validity
      if (jquery__WEBPACK_IMPORTED_MODULE_0___default()(_this.shippingEstimator + " select[name=\"shipping-country\"]").val()) {
        _this.shippingValidator.performCheck();
      }

      if (_this.shippingValidator.areAll('valid')) {
        return;
      }

      event.preventDefault();
    });
    this.bindValidation();
    this.bindStateValidation();
    this.bindUPSRates();
  };

  _proto.bindValidation = function bindValidation() {
    this.shippingValidator.add([{
      selector: this.shippingEstimator + " select[name=\"shipping-country\"]",
      validate: function validate(cb, val) {
        var countryId = Number(val);
        var result = countryId !== 0 && !Number.isNaN(countryId);
        cb(result);
      },
      errorMessage: 'The \'Country\' field cannot be blank.'
    }]);
  };

  _proto.bindStateValidation = function bindStateValidation() {
    var _this2 = this;

    this.shippingValidator.add([{
      selector: jquery__WEBPACK_IMPORTED_MODULE_0___default()(this.shippingEstimator + " select[name=\"shipping-state\"]"),
      validate: function validate(cb) {
        var result;
        var $ele = jquery__WEBPACK_IMPORTED_MODULE_0___default()(_this2.shippingEstimator + " select[name=\"shipping-state\"]");

        if ($ele.length) {
          var eleVal = $ele.val();
          result = eleVal && eleVal.length && eleVal !== 'State/province';
        }

        cb(result);
      },
      errorMessage: 'The \'State/Province\' field cannot be blank.'
    }]);
  }
  /**
   * Toggle between default shipping and ups shipping rates
   */
  ;

  _proto.bindUPSRates = function bindUPSRates() {
    var UPSRateToggle = '.estimator-form-toggleUPSRate';
    jquery__WEBPACK_IMPORTED_MODULE_0___default()('body').on('click', UPSRateToggle, function (event) {
      var $estimatorFormUps = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.estimator-form--ups');
      var $estimatorFormDefault = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.estimator-form--default');
      event.preventDefault();
      $estimatorFormUps.toggleClass('u-hiddenVisually');
      $estimatorFormDefault.toggleClass('u-hiddenVisually');
    });
  };

  _proto.bindStateCountryChange = function bindStateCountryChange() {
    var _this3 = this;

    var $last; // Requests the states for a country with AJAX

    Object(_common_state_country__WEBPACK_IMPORTED_MODULE_1__["default"])(this.$state, this.context, {
      useIdForStates: true
    }, function (err, field) {
      if (err) {
        _global_sweet_alert__WEBPACK_IMPORTED_MODULE_5__["default"].fire({
          text: err,
          icon: 'error'
        });
        throw new Error(err);
      }

      var $field = jquery__WEBPACK_IMPORTED_MODULE_0___default()(field);

      if (_this3.shippingValidator.getStatus(_this3.$state) !== 'undefined') {
        _this3.shippingValidator.remove(_this3.$state);
      }

      if ($last) {
        _this3.shippingValidator.remove($last);
      }

      if ($field.is('select')) {
        $last = field;

        _this3.bindStateValidation();
      } else {
        $field.attr('placeholder', 'State/province');
        _common_form_utils__WEBPACK_IMPORTED_MODULE_4__["Validators"].cleanUpStateValidation(field);
      } // When you change a country, you swap the state/province between an input and a select dropdown
      // Not all countries require the province to be filled
      // We have to remove this class when we swap since nod validation doesn't cleanup for us


      jquery__WEBPACK_IMPORTED_MODULE_0___default()(_this3.shippingEstimator).find('.form-field--success').removeClass('form-field--success');
    });
  };

  _proto.bindEstimatorEvents = function bindEstimatorEvents() {
    var $estimatorContainer = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.shipping-estimator');
    var $estimatorForm = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.estimator-form');
    $estimatorForm.on('submit', function (event) {
      var params = {
        country_id: jquery__WEBPACK_IMPORTED_MODULE_0___default()('[name="shipping-country"]', $estimatorForm).val(),
        state_id: jquery__WEBPACK_IMPORTED_MODULE_0___default()('[name="shipping-state"]', $estimatorForm).val(),
        city: jquery__WEBPACK_IMPORTED_MODULE_0___default()('[name="shipping-city"]', $estimatorForm).val(),
        zip_code: jquery__WEBPACK_IMPORTED_MODULE_0___default()('[name="shipping-zip"]', $estimatorForm).val()
      };
      event.preventDefault();
      _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_3__["default"].api.cart.getShippingQuotes(params, 'cart/shipping-quotes', function (err, response) {
        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.shipping-quotes').html(response.content); // bind the select button

        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.select-shipping-quote').on('click', function (clickEvent) {
          var quoteId = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.shipping-quote:checked').val();
          clickEvent.preventDefault();
          _bigcommerce_stencil_utils__WEBPACK_IMPORTED_MODULE_3__["default"].api.cart.submitShippingQuote(quoteId, function () {
            window.location.reload();
          });
        });
      });
    });
    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.shipping-estimate-show').on('click', function (event) {
      event.preventDefault();
      jquery__WEBPACK_IMPORTED_MODULE_0___default()(event.currentTarget).hide();
      $estimatorContainer.removeClass('u-hiddenVisually');
      jquery__WEBPACK_IMPORTED_MODULE_0___default()('.shipping-estimate-hide').show();
    });
    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.shipping-estimate-hide').on('click', function (event) {
      event.preventDefault();
      $estimatorContainer.addClass('u-hiddenVisually');
      jquery__WEBPACK_IMPORTED_MODULE_0___default()('.shipping-estimate-show').show();
      jquery__WEBPACK_IMPORTED_MODULE_0___default()('.shipping-estimate-hide').hide();
    });
  };

  return ShippingEstimator;
}();



/***/ }),

/***/ "./assets/js/theme/common/gift-certificate-validator.js":
/*!**************************************************************!*\
  !*** ./assets/js/theme/common/gift-certificate-validator.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function (cert) {
  if (typeof cert !== 'string') {
    return false;
  } // Add any custom gift certificate validation logic here


  return true;
});

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvdGhlbWUvY2FydC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvdGhlbWUvY2FydC9zaGlwcGluZy1lc3RpbWF0b3IuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3RoZW1lL2NvbW1vbi9naWZ0LWNlcnRpZmljYXRlLXZhbGlkYXRvci5qcyJdLCJuYW1lcyI6WyJzZXRTaGlwcGluZ01lc3NhZ2UiLCJpbnYiLCJxdWFudGl0eSIsImlzRGVmYXVsdCIsImxlYWR0aW1lIiwidHJpZ2dlciIsIm5iZE1zZyIsImxlYWR0aW1lTXNnIiwiYXZhaWxhYmxlIiwicXR5IiwiZGlmZiIsIm1lc3NhZ2UiLCJDYXJ0Iiwib25SZWFkeSIsIiRjYXJ0Q29udGVudCIsIiQiLCIkY2FydE1lc3NhZ2VzIiwiJGNhcnRUb3RhbHMiLCIkb3ZlcmxheSIsImhpZGUiLCJiaW5kRXZlbnRzIiwiZWFjaCIsIml0ZW1JZCIsImRhdGEiLCJ2YWwiLCJwYXJzZUludCIsInRleHQiLCJtc2ciLCJjb25zb2xlIiwibG9nIiwic2hvdyIsImNhcnRVcGRhdGUiLCIkdGFyZ2V0IiwiJGVsIiwib2xkUXR5IiwibWF4UXR5IiwibWluUXR5IiwibWluRXJyb3IiLCJtYXhFcnJvciIsIm5ld1F0eSIsInN3YWwiLCJmaXJlIiwiaWNvbiIsInV0aWxzIiwiYXBpIiwiY2FydCIsIml0ZW1VcGRhdGUiLCJlcnIiLCJyZXNwb25zZSIsInN0YXR1cyIsInJlbW92ZSIsInJlZnJlc2hDb250ZW50IiwiZXJyb3JzIiwiam9pbiIsImNhcnRVcGRhdGVRdHlUZXh0Q2hhbmdlIiwicHJlVmFsIiwiTnVtYmVyIiwiaW52YWxpZEVudHJ5IiwiY2FydFJlbW92ZUl0ZW0iLCJpdGVtUmVtb3ZlIiwiY2FydEVkaXRPcHRpb25zIiwibW9kYWwiLCJkZWZhdWx0TW9kYWwiLCJvcHRpb25zIiwidGVtcGxhdGUiLCJvcGVuIiwicHJvZHVjdEF0dHJpYnV0ZXMiLCJjb25maWd1cmVJbkNhcnQiLCJ1cGRhdGVDb250ZW50IiwiY29udGVudCIsImJpbmRHaWZ0V3JhcHBpbmdGb3JtIiwiaG9va3MiLCJvbiIsImV2ZW50Iiwib3B0aW9uIiwiJGNoYW5nZWRPcHRpb24iLCIkZm9ybSIsInBhcmVudHMiLCIkc3VibWl0IiwiJG1lc3NhZ2VCb3giLCJpdGVtIiwiYXR0ciIsIm9wdGlvbkNoYW5nZSIsInNlcmlhbGl6ZSIsInJlc3VsdCIsInB1cmNoYXNpbmdfbWVzc2FnZSIsInByb3AiLCJwdXJjaGFzYWJsZSIsImluc3RvY2siLCIkY2FydEl0ZW1zUm93cyIsIiRjYXJ0UGFnZVRpdGxlIiwidG90YWxzIiwicGFnZVRpdGxlIiwic3RhdHVzTWVzc2FnZXMiLCJsZW5ndGgiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsInJlbG9hZCIsImdldENvbnRlbnQiLCJodG1sIiwicmVwbGFjZVdpdGgiLCJiaW5kQ2FydEV2ZW50cyIsImRlYm91bmNlVGltZW91dCIsImN1cnJlbnRUYXJnZXQiLCJwcmV2ZW50RGVmYXVsdCIsIm9uUXR5Rm9jdXMiLCJ2YWx1ZSIsImNoYW5nZSIsInN0cmluZyIsInNob3dDYW5jZWxCdXR0b24iLCJ0aGVuIiwiYmluZFByb21vQ29kZUV2ZW50cyIsIiRjb3Vwb25Db250YWluZXIiLCIkY291cG9uRm9ybSIsIiRjb2RlSW5wdXQiLCJjb2RlIiwiYXBwbHlDb2RlIiwiYmluZEdpZnRDZXJ0aWZpY2F0ZUV2ZW50cyIsIiRjZXJ0Q29udGFpbmVyIiwiJGNlcnRGb3JtIiwiJGNlcnRJbnB1dCIsInRvZ2dsZSIsImdpZnRDZXJ0Q2hlY2siLCJhcHBseUdpZnRDZXJ0aWZpY2F0ZSIsInJlc3AiLCJiaW5kR2lmdFdyYXBwaW5nRXZlbnRzIiwiZ2V0SXRlbUdpZnRXcmFwcGluZ09wdGlvbnMiLCIkc2VsZWN0IiwiaWQiLCJpbmRleCIsImFsbG93TWVzc2FnZSIsImZpbmQiLCJ0b2dnbGVWaWV3cyIsIiRzaW5nbGVGb3JtIiwiJG11bHRpRm9ybSIsInNoaXBwaW5nRXN0aW1hdG9yIiwiU2hpcHBpbmdFc3RpbWF0b3IiLCJQYWdlTWFuYWdlciIsIiRlbGVtZW50IiwiJHN0YXRlIiwiaW5pdEZvcm1WYWxpZGF0aW9uIiwiYmluZFN0YXRlQ291bnRyeUNoYW5nZSIsImJpbmRFc3RpbWF0b3JFdmVudHMiLCJzaGlwcGluZ1ZhbGlkYXRvciIsIm5vZCIsInN1Ym1pdCIsInBlcmZvcm1DaGVjayIsImFyZUFsbCIsImJpbmRWYWxpZGF0aW9uIiwiYmluZFN0YXRlVmFsaWRhdGlvbiIsImJpbmRVUFNSYXRlcyIsImFkZCIsInNlbGVjdG9yIiwidmFsaWRhdGUiLCJjYiIsImNvdW50cnlJZCIsImlzTmFOIiwiZXJyb3JNZXNzYWdlIiwiJGVsZSIsImVsZVZhbCIsIlVQU1JhdGVUb2dnbGUiLCIkZXN0aW1hdG9yRm9ybVVwcyIsIiRlc3RpbWF0b3JGb3JtRGVmYXVsdCIsInRvZ2dsZUNsYXNzIiwiJGxhc3QiLCJzdGF0ZUNvdW50cnkiLCJjb250ZXh0IiwidXNlSWRGb3JTdGF0ZXMiLCJmaWVsZCIsIkVycm9yIiwiJGZpZWxkIiwiZ2V0U3RhdHVzIiwiaXMiLCJWYWxpZGF0b3JzIiwiY2xlYW5VcFN0YXRlVmFsaWRhdGlvbiIsInJlbW92ZUNsYXNzIiwiJGVzdGltYXRvckNvbnRhaW5lciIsIiRlc3RpbWF0b3JGb3JtIiwicGFyYW1zIiwiY291bnRyeV9pZCIsInN0YXRlX2lkIiwiY2l0eSIsInppcF9jb2RlIiwiZ2V0U2hpcHBpbmdRdW90ZXMiLCJjbGlja0V2ZW50IiwicXVvdGVJZCIsInN1Ym1pdFNoaXBwaW5nUXVvdGUiLCJhZGRDbGFzcyIsImNlcnQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBU0Esa0JBQVQsQ0FBNEJDLEdBQTVCLEVBQWlDQyxRQUFqQyxFQUEyQ0MsU0FBM0MsRUFBc0Q7QUFDbEQsTUFBSUMsUUFBUSxHQUFHLENBQWY7QUFDQSxNQUFJQyxPQUFPLEdBQUcsRUFBZDtBQUNBLE1BQUlDLE1BQU0sR0FBRyxzQ0FBYjtBQUNBLE1BQUlDLFdBQVcsR0FBRyw0QkFBNEIsSUFBSUgsUUFBaEMsSUFBNEMsZ0JBQTlEO0FBQ0EsTUFBSUksU0FBUyxHQUFHUCxHQUFoQjtBQUVBLE1BQUlRLEdBQUcsR0FBR1AsUUFBVjtBQUNBLE1BQUlRLElBQUksR0FBR0YsU0FBUyxHQUFHQyxHQUF2QjtBQUNBLE1BQUlFLE9BQU8sR0FBRyxFQUFkOztBQUVBLE1BQUlSLFNBQUosRUFBZTtBQUNYO0FBQ0EsUUFBSUssU0FBUyxHQUFHLENBQVosSUFBaUJOLFFBQVEsSUFBSU0sU0FBakMsRUFBNkM7QUFDekNHLGFBQU8sR0FBRyx5QkFBVjtBQUNILEtBRkQsTUFFTyxJQUFJSCxTQUFTLElBQUksQ0FBYixJQUFrQkUsSUFBSSxHQUFHLENBQUMsRUFBOUIsRUFBa0M7QUFDckNDLGFBQU8sR0FBRyxlQUFnQixJQUFJUCxRQUFwQixJQUFnQyxnQkFBMUM7QUFDSCxLQUZNLE1BRUEsSUFBSUksU0FBUyxJQUFJLENBQWIsSUFBa0JFLElBQUksSUFBSSxDQUFDLEVBQS9CLEVBQW1DO0FBQ3RDQyxhQUFPLEdBQUcsbUNBQVY7QUFDSDtBQUVKLEdBVkQsTUFVTztBQUVILFFBQUlELElBQUksSUFBSSxDQUFaLEVBQWdCO0FBQ1osVUFBSUEsSUFBSSxHQUFHRixTQUFQLElBQW9CQSxTQUFTLEdBQUcsQ0FBcEMsRUFBdUM7QUFDbkNHLGVBQU8sR0FBR0YsR0FBRyxHQUFHSCxNQUFoQjtBQUNIO0FBRUosS0FMRCxNQUtRO0FBQ0osVUFBSUksSUFBSSxHQUFHTCxPQUFQLElBQW1CRyxTQUFTLEdBQUcsQ0FBbkMsRUFBdUM7QUFDbkMsWUFBSUUsSUFBSSxJQUFJLENBQUNMLE9BQWIsRUFBc0I7QUFDbEJNLGlCQUFPLEdBQUcsbUNBQVY7QUFDSCxTQUZELE1BRU87QUFDSEEsaUJBQU8sR0FBR0gsU0FBUyxHQUFHRixNQUFaLEdBQW9CLElBQXBCLEdBQTBCLENBQUNJLElBQTNCLEdBQWtDSCxXQUE1QztBQUNIO0FBRUosT0FQRCxNQU9PLElBQUksQ0FBQ0csSUFBRCxHQUFRTCxPQUFSLElBQW9CRyxTQUFTLElBQUksQ0FBckMsRUFBMEM7QUFDN0NHLGVBQU8sR0FBR0osV0FBVjtBQUVILE9BSE0sTUFHQTtBQUNISSxlQUFPLEdBQUcsbUNBQVY7QUFFSDtBQUNKO0FBRUosR0E3Q2lELENBK0NsRDs7O0FBRUEsU0FBT0EsT0FBUDtBQUVIOztJQUdvQkMsSTs7Ozs7Ozs7O1NBQ2pCQyxPLEdBQUEsbUJBQVU7QUFDTixTQUFLQyxZQUFMLEdBQW9CQyw2Q0FBQyxDQUFDLHFCQUFELENBQXJCO0FBQ0EsU0FBS0MsYUFBTCxHQUFxQkQsNkNBQUMsQ0FBQyxvQkFBRCxDQUF0QjtBQUNBLFNBQUtFLFdBQUwsR0FBbUJGLDZDQUFDLENBQUMsb0JBQUQsQ0FBcEI7QUFDQSxTQUFLRyxRQUFMLEdBQWdCSCw2Q0FBQyxDQUFDLDZCQUFELENBQUQsQ0FDWEksSUFEVyxFQUFoQixDQUpNLENBS087O0FBRWIsU0FBS0MsVUFBTDtBQUVGTCxpREFBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEJNLElBQTFCLENBQWdDLFlBQVc7QUFDdkMsVUFBSUMsTUFBTSxHQUFHUCw2Q0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRUSxJQUFSLENBQWEsWUFBYixDQUFiO0FBQ0EsVUFBSWQsR0FBRyxHQUFHTSw2Q0FBQyxXQUFTTyxNQUFULENBQUQsQ0FBb0JFLEdBQXBCLEVBQVY7QUFFQSxVQUFJdkIsR0FBRyxHQUFHd0IsUUFBUSxDQUFDViw2Q0FBQyx1QkFBcUJPLE1BQXJCLENBQUQsQ0FBZ0NJLElBQWhDLEVBQUQsQ0FBbEI7QUFFQSxVQUFJQyxHQUFHLEdBQUczQixrQkFBa0IsQ0FBQ0MsR0FBRCxFQUFNUSxHQUFOLEVBQVcsS0FBWCxDQUE1QjtBQUNBbUIsYUFBTyxDQUFDQyxHQUFSLENBQVlGLEdBQUcsR0FBRyxVQUFsQjtBQUNBWixtREFBQywwQkFBd0JPLE1BQXhCLENBQUQsQ0FBbUNJLElBQW5DLENBQXdDQyxHQUF4QztBQUNBWixtREFBQywwQkFBd0JPLE1BQXhCLENBQUQsQ0FBbUNRLElBQW5DO0FBQ0YsS0FWRjtBQWFELEc7O1NBRURDLFUsR0FBQSxvQkFBV0MsT0FBWCxFQUFvQjtBQUFBOztBQUNoQixRQUFNVixNQUFNLEdBQUdVLE9BQU8sQ0FBQ1QsSUFBUixDQUFhLFlBQWIsQ0FBZjtBQUNBLFFBQU1VLEdBQUcsR0FBR2xCLDZDQUFDLFdBQVNPLE1BQVQsQ0FBYjtBQUNBLFFBQU1ZLE1BQU0sR0FBR1QsUUFBUSxDQUFDUSxHQUFHLENBQUNULEdBQUosRUFBRCxFQUFZLEVBQVosQ0FBdkI7QUFDQSxRQUFNVyxNQUFNLEdBQUdWLFFBQVEsQ0FBQ1EsR0FBRyxDQUFDVixJQUFKLENBQVMsYUFBVCxDQUFELEVBQTBCLEVBQTFCLENBQXZCO0FBQ0EsUUFBTWEsTUFBTSxHQUFHWCxRQUFRLENBQUNRLEdBQUcsQ0FBQ1YsSUFBSixDQUFTLGFBQVQsQ0FBRCxFQUEwQixFQUExQixDQUF2QjtBQUNBLFFBQU1jLFFBQVEsR0FBR0osR0FBRyxDQUFDVixJQUFKLENBQVMsa0JBQVQsQ0FBakI7QUFDQSxRQUFNZSxRQUFRLEdBQUdMLEdBQUcsQ0FBQ1YsSUFBSixDQUFTLGtCQUFULENBQWpCO0FBQ0EsUUFBTWdCLE1BQU0sR0FBR1AsT0FBTyxDQUFDVCxJQUFSLENBQWEsUUFBYixNQUEyQixLQUEzQixHQUFtQ1csTUFBTSxHQUFHLENBQTVDLEdBQWdEQSxNQUFNLEdBQUcsQ0FBeEUsQ0FSZ0IsQ0FTaEI7O0FBQ0EsUUFBSUssTUFBTSxHQUFHSCxNQUFiLEVBQXFCO0FBQ2pCLGFBQU9JLDJEQUFJLENBQUNDLElBQUwsQ0FBVTtBQUNiZixZQUFJLEVBQUVXLFFBRE87QUFFYkssWUFBSSxFQUFFO0FBRk8sT0FBVixDQUFQO0FBSUgsS0FMRCxNQUtPLElBQUlQLE1BQU0sR0FBRyxDQUFULElBQWNJLE1BQU0sR0FBR0osTUFBM0IsRUFBbUM7QUFDdEMsYUFBT0ssMkRBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ2JmLFlBQUksRUFBRVksUUFETztBQUViSSxZQUFJLEVBQUU7QUFGTyxPQUFWLENBQVA7QUFJSDs7QUFFRCxTQUFLeEIsUUFBTCxDQUFjWSxJQUFkO0FBRUFhLHNFQUFLLENBQUNDLEdBQU4sQ0FBVUMsSUFBVixDQUFlQyxVQUFmLENBQTBCeEIsTUFBMUIsRUFBa0NpQixNQUFsQyxFQUEwQyxVQUFDUSxHQUFELEVBQU1DLFFBQU4sRUFBbUI7QUFDekQsV0FBSSxDQUFDOUIsUUFBTCxDQUFjQyxJQUFkOztBQUVBLFVBQUk2QixRQUFRLENBQUN6QixJQUFULENBQWMwQixNQUFkLEtBQXlCLFNBQTdCLEVBQXdDO0FBQ3BDO0FBQ0EsWUFBTUMsTUFBTSxHQUFJWCxNQUFNLEtBQUssQ0FBM0I7O0FBRUEsYUFBSSxDQUFDWSxjQUFMLENBQW9CRCxNQUFwQjtBQUNILE9BTEQsTUFLTztBQUNIakIsV0FBRyxDQUFDVCxHQUFKLENBQVFVLE1BQVI7QUFDQU0sbUVBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ05mLGNBQUksRUFBRXNCLFFBQVEsQ0FBQ3pCLElBQVQsQ0FBYzZCLE1BQWQsQ0FBcUJDLElBQXJCLENBQTBCLElBQTFCLENBREE7QUFFTlgsY0FBSSxFQUFFO0FBRkEsU0FBVjtBQUlIO0FBQ0osS0FmRDtBQWdCSCxHOztTQUVEWSx1QixHQUFBLGlDQUF3QnRCLE9BQXhCLEVBQWlDdUIsTUFBakMsRUFBZ0Q7QUFBQTs7QUFBQSxRQUFmQSxNQUFlO0FBQWZBLFlBQWUsR0FBTixJQUFNO0FBQUE7O0FBQzVDLFFBQU1qQyxNQUFNLEdBQUdVLE9BQU8sQ0FBQ1QsSUFBUixDQUFhLFlBQWIsQ0FBZjtBQUNBLFFBQU1VLEdBQUcsR0FBR2xCLDZDQUFDLFdBQVNPLE1BQVQsQ0FBYjtBQUNBLFFBQU1hLE1BQU0sR0FBR1YsUUFBUSxDQUFDUSxHQUFHLENBQUNWLElBQUosQ0FBUyxhQUFULENBQUQsRUFBMEIsRUFBMUIsQ0FBdkI7QUFDQSxRQUFNYSxNQUFNLEdBQUdYLFFBQVEsQ0FBQ1EsR0FBRyxDQUFDVixJQUFKLENBQVMsYUFBVCxDQUFELEVBQTBCLEVBQTFCLENBQXZCO0FBQ0EsUUFBTVcsTUFBTSxHQUFHcUIsTUFBTSxLQUFLLElBQVgsR0FBa0JBLE1BQWxCLEdBQTJCbkIsTUFBMUM7QUFDQSxRQUFNQyxRQUFRLEdBQUdKLEdBQUcsQ0FBQ1YsSUFBSixDQUFTLGtCQUFULENBQWpCO0FBQ0EsUUFBTWUsUUFBUSxHQUFHTCxHQUFHLENBQUNWLElBQUosQ0FBUyxrQkFBVCxDQUFqQjtBQUNBLFFBQU1nQixNQUFNLEdBQUdkLFFBQVEsQ0FBQytCLE1BQU0sQ0FBQ3ZCLEdBQUcsQ0FBQ1QsR0FBSixFQUFELENBQVAsRUFBb0IsRUFBcEIsQ0FBdkI7QUFDQSxRQUFJaUMsWUFBSixDQVQ0QyxDQVc1Qzs7QUFDQSxRQUFJLENBQUNsQixNQUFMLEVBQWE7QUFDVGtCLGtCQUFZLEdBQUd4QixHQUFHLENBQUNULEdBQUosRUFBZjtBQUNBUyxTQUFHLENBQUNULEdBQUosQ0FBUVUsTUFBUjtBQUNBLGFBQU9NLDJEQUFJLENBQUNDLElBQUwsQ0FBVTtBQUNiZixZQUFJLEVBQUsrQixZQUFMLDBCQURTO0FBRWJmLFlBQUksRUFBRTtBQUZPLE9BQVYsQ0FBUDtBQUlILEtBUEQsTUFPTyxJQUFJSCxNQUFNLEdBQUdILE1BQWIsRUFBcUI7QUFDeEJILFNBQUcsQ0FBQ1QsR0FBSixDQUFRVSxNQUFSO0FBQ0EsYUFBT00sMkRBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ2JmLFlBQUksRUFBRVcsUUFETztBQUViSyxZQUFJLEVBQUU7QUFGTyxPQUFWLENBQVA7QUFJSCxLQU5NLE1BTUEsSUFBSVAsTUFBTSxHQUFHLENBQVQsSUFBY0ksTUFBTSxHQUFHSixNQUEzQixFQUFtQztBQUN0Q0YsU0FBRyxDQUFDVCxHQUFKLENBQVFVLE1BQVI7QUFDQSxhQUFPTSwyREFBSSxDQUFDQyxJQUFMLENBQVU7QUFDYmYsWUFBSSxFQUFFWSxRQURPO0FBRWJJLFlBQUksRUFBRTtBQUZPLE9BQVYsQ0FBUDtBQUlIOztBQUVELFNBQUt4QixRQUFMLENBQWNZLElBQWQ7QUFDQWEsc0VBQUssQ0FBQ0MsR0FBTixDQUFVQyxJQUFWLENBQWVDLFVBQWYsQ0FBMEJ4QixNQUExQixFQUFrQ2lCLE1BQWxDLEVBQTBDLFVBQUNRLEdBQUQsRUFBTUMsUUFBTixFQUFtQjtBQUN6RCxZQUFJLENBQUM5QixRQUFMLENBQWNDLElBQWQ7O0FBRUEsVUFBSTZCLFFBQVEsQ0FBQ3pCLElBQVQsQ0FBYzBCLE1BQWQsS0FBeUIsU0FBN0IsRUFBd0M7QUFDcEM7QUFDQSxZQUFNQyxNQUFNLEdBQUlYLE1BQU0sS0FBSyxDQUEzQjs7QUFFQSxjQUFJLENBQUNZLGNBQUwsQ0FBb0JELE1BQXBCO0FBQ0gsT0FMRCxNQUtPO0FBQ0hqQixXQUFHLENBQUNULEdBQUosQ0FBUVUsTUFBUjtBQUNBTSxtRUFBSSxDQUFDQyxJQUFMLENBQVU7QUFDTmYsY0FBSSxFQUFFc0IsUUFBUSxDQUFDekIsSUFBVCxDQUFjNkIsTUFBZCxDQUFxQkMsSUFBckIsQ0FBMEIsSUFBMUIsQ0FEQTtBQUVOWCxjQUFJLEVBQUU7QUFGQSxTQUFWO0FBSUg7QUFDSixLQWZEO0FBZ0JILEc7O1NBRURnQixjLEdBQUEsd0JBQWVwQyxNQUFmLEVBQXVCO0FBQUE7O0FBQ25CLFNBQUtKLFFBQUwsQ0FBY1ksSUFBZDtBQUNBYSxzRUFBSyxDQUFDQyxHQUFOLENBQVVDLElBQVYsQ0FBZWMsVUFBZixDQUEwQnJDLE1BQTFCLEVBQWtDLFVBQUN5QixHQUFELEVBQU1DLFFBQU4sRUFBbUI7QUFDakQsVUFBSUEsUUFBUSxDQUFDekIsSUFBVCxDQUFjMEIsTUFBZCxLQUF5QixTQUE3QixFQUF3QztBQUNwQyxjQUFJLENBQUNFLGNBQUwsQ0FBb0IsSUFBcEI7QUFDSCxPQUZELE1BRU87QUFDSFgsbUVBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ05mLGNBQUksRUFBRXNCLFFBQVEsQ0FBQ3pCLElBQVQsQ0FBYzZCLE1BQWQsQ0FBcUJDLElBQXJCLENBQTBCLElBQTFCLENBREE7QUFFTlgsY0FBSSxFQUFFO0FBRkEsU0FBVjtBQUlIO0FBQ0osS0FURDtBQVVILEc7O1NBRURrQixlLEdBQUEseUJBQWdCdEMsTUFBaEIsRUFBd0I7QUFBQTs7QUFDcEIsUUFBTXVDLEtBQUssR0FBR0Msa0VBQVksRUFBMUI7QUFDQSxRQUFNQyxPQUFPLEdBQUc7QUFDWkMsY0FBUSxFQUFFO0FBREUsS0FBaEI7QUFJQUgsU0FBSyxDQUFDSSxJQUFOO0FBRUF0QixzRUFBSyxDQUFDQyxHQUFOLENBQVVzQixpQkFBVixDQUE0QkMsZUFBNUIsQ0FBNEM3QyxNQUE1QyxFQUFvRHlDLE9BQXBELEVBQTZELFVBQUNoQixHQUFELEVBQU1DLFFBQU4sRUFBbUI7QUFDNUVhLFdBQUssQ0FBQ08sYUFBTixDQUFvQnBCLFFBQVEsQ0FBQ3FCLE9BQTdCOztBQUVBLFlBQUksQ0FBQ0Msb0JBQUw7QUFDSCxLQUpEO0FBTUEzQixzRUFBSyxDQUFDNEIsS0FBTixDQUFZQyxFQUFaLENBQWUsdUJBQWYsRUFBd0MsVUFBQ0MsS0FBRCxFQUFRQyxNQUFSLEVBQW1CO0FBQ3ZELFVBQU1DLGNBQWMsR0FBRzVELDZDQUFDLENBQUMyRCxNQUFELENBQXhCO0FBQ0EsVUFBTUUsS0FBSyxHQUFHRCxjQUFjLENBQUNFLE9BQWYsQ0FBdUIsTUFBdkIsQ0FBZDtBQUNBLFVBQU1DLE9BQU8sR0FBRy9ELDZDQUFDLENBQUMsY0FBRCxFQUFpQjZELEtBQWpCLENBQWpCO0FBQ0EsVUFBTUcsV0FBVyxHQUFHaEUsNkNBQUMsQ0FBQyxrQkFBRCxDQUFyQjtBQUNBLFVBQU1pRSxJQUFJLEdBQUdqRSw2Q0FBQyxDQUFDLGtCQUFELEVBQXFCNkQsS0FBckIsQ0FBRCxDQUE2QkssSUFBN0IsQ0FBa0MsT0FBbEMsQ0FBYjtBQUVBdEMsd0VBQUssQ0FBQ0MsR0FBTixDQUFVc0IsaUJBQVYsQ0FBNEJnQixZQUE1QixDQUF5Q0YsSUFBekMsRUFBK0NKLEtBQUssQ0FBQ08sU0FBTixFQUEvQyxFQUFrRSxVQUFDcEMsR0FBRCxFQUFNcUMsTUFBTixFQUFpQjtBQUMvRSxZQUFNN0QsSUFBSSxHQUFHNkQsTUFBTSxDQUFDN0QsSUFBUCxJQUFlLEVBQTVCOztBQUVBLFlBQUl3QixHQUFKLEVBQVM7QUFDTFAscUVBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ05mLGdCQUFJLEVBQUVxQixHQURBO0FBRU5MLGdCQUFJLEVBQUU7QUFGQSxXQUFWO0FBSUEsaUJBQU8sS0FBUDtBQUNIOztBQUVELFlBQUluQixJQUFJLENBQUM4RCxrQkFBVCxFQUE2QjtBQUN6QnRFLHVEQUFDLENBQUMsb0JBQUQsRUFBdUJnRSxXQUF2QixDQUFELENBQXFDckQsSUFBckMsQ0FBMENILElBQUksQ0FBQzhELGtCQUEvQztBQUNBUCxpQkFBTyxDQUFDUSxJQUFSLENBQWEsVUFBYixFQUF5QixJQUF6QjtBQUNBUCxxQkFBVyxDQUFDakQsSUFBWjtBQUNILFNBSkQsTUFJTztBQUNIZ0QsaUJBQU8sQ0FBQ1EsSUFBUixDQUFhLFVBQWIsRUFBeUIsS0FBekI7QUFDQVAscUJBQVcsQ0FBQzVELElBQVo7QUFDSDs7QUFFRCxZQUFJLENBQUNJLElBQUksQ0FBQ2dFLFdBQU4sSUFBcUIsQ0FBQ2hFLElBQUksQ0FBQ2lFLE9BQS9CLEVBQXdDO0FBQ3BDVixpQkFBTyxDQUFDUSxJQUFSLENBQWEsVUFBYixFQUF5QixJQUF6QjtBQUNILFNBRkQsTUFFTztBQUNIUixpQkFBTyxDQUFDUSxJQUFSLENBQWEsVUFBYixFQUF5QixLQUF6QjtBQUNIO0FBQ0osT0F6QkQ7QUEwQkgsS0FqQ0Q7QUFrQ0gsRzs7U0FFRG5DLGMsR0FBQSx3QkFBZUQsTUFBZixFQUF1QjtBQUFBOztBQUNuQixRQUFNdUMsY0FBYyxHQUFHMUUsNkNBQUMsQ0FBQyxpQkFBRCxFQUFvQixLQUFLRCxZQUF6QixDQUF4QjtBQUNBLFFBQU00RSxjQUFjLEdBQUczRSw2Q0FBQyxDQUFDLHdCQUFELENBQXhCO0FBQ0EsUUFBTWdELE9BQU8sR0FBRztBQUNaQyxjQUFRLEVBQUU7QUFDTkssZUFBTyxFQUFFLGNBREg7QUFFTnNCLGNBQU0sRUFBRSxhQUZGO0FBR05DLGlCQUFTLEVBQUUsaUJBSEw7QUFJTkMsc0JBQWMsRUFBRTtBQUpWO0FBREUsS0FBaEI7QUFTQSxTQUFLM0UsUUFBTCxDQUFjWSxJQUFkLEdBWm1CLENBY25COztBQUNBLFFBQUlvQixNQUFNLElBQUl1QyxjQUFjLENBQUNLLE1BQWYsS0FBMEIsQ0FBeEMsRUFBMkM7QUFDdkMsYUFBT0MsTUFBTSxDQUFDQyxRQUFQLENBQWdCQyxNQUFoQixFQUFQO0FBQ0g7O0FBRUR0RCxzRUFBSyxDQUFDQyxHQUFOLENBQVVDLElBQVYsQ0FBZXFELFVBQWYsQ0FBMEJuQyxPQUExQixFQUFtQyxVQUFDaEIsR0FBRCxFQUFNQyxRQUFOLEVBQW1CO0FBQ2xELFlBQUksQ0FBQ2xDLFlBQUwsQ0FBa0JxRixJQUFsQixDQUF1Qm5ELFFBQVEsQ0FBQ3FCLE9BQWhDOztBQUNBLFlBQUksQ0FBQ3BELFdBQUwsQ0FBaUJrRixJQUFqQixDQUFzQm5ELFFBQVEsQ0FBQzJDLE1BQS9COztBQUNBLFlBQUksQ0FBQzNFLGFBQUwsQ0FBbUJtRixJQUFuQixDQUF3Qm5ELFFBQVEsQ0FBQzZDLGNBQWpDOztBQUVBSCxvQkFBYyxDQUFDVSxXQUFmLENBQTJCcEQsUUFBUSxDQUFDNEMsU0FBcEM7O0FBQ0EsWUFBSSxDQUFDeEUsVUFBTDs7QUFDQSxZQUFJLENBQUNGLFFBQUwsQ0FBY0MsSUFBZDs7QUFFQSxVQUFNakIsUUFBUSxHQUFHYSw2Q0FBQyxDQUFDLHNCQUFELEVBQXlCLE1BQUksQ0FBQ0QsWUFBOUIsQ0FBRCxDQUE2Q1MsSUFBN0MsQ0FBa0QsY0FBbEQsS0FBcUUsQ0FBdEY7QUFFQVIsbURBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVVYsT0FBVixDQUFrQixzQkFBbEIsRUFBMENILFFBQTFDLEVBWGtELENBYWxEOztBQUNBYSxtREFBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEJNLElBQTFCLENBQWdDLFlBQVk7QUFDeEMsWUFBSUMsTUFBTSxHQUFHUCw2Q0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRUSxJQUFSLENBQWEsWUFBYixDQUFiO0FBQ0EsWUFBSXRCLEdBQUcsR0FBR3dCLFFBQVEsQ0FBQ1YsNkNBQUMsdUJBQXFCTyxNQUFyQixDQUFELENBQWdDSSxJQUFoQyxFQUFELENBQWxCO0FBQ0EsWUFBSWpCLEdBQUcsR0FBR00sNkNBQUMsV0FBU08sTUFBVCxDQUFELENBQW9CRSxHQUFwQixFQUFWO0FBQ0EsWUFBSUcsR0FBRyxHQUFHM0Isa0JBQWtCLENBQUNDLEdBQUQsRUFBTVEsR0FBTixFQUFXLEtBQVgsQ0FBNUIsQ0FKd0MsQ0FLM0M7O0FBQ0dNLHFEQUFDLDBCQUF3Qk8sTUFBeEIsQ0FBRCxDQUFtQ0ksSUFBbkMsQ0FBd0NDLEdBQXhDO0FBQ0FaLHFEQUFDLDBCQUF3Qk8sTUFBeEIsQ0FBRCxDQUFtQ1EsSUFBbkM7QUFFSCxPQVREO0FBV0gsS0F6QkQ7QUEwQkgsRzs7U0FFRHVFLGMsR0FBQSwwQkFBaUI7QUFBQTs7QUFDYixRQUFNQyxlQUFlLEdBQUcsR0FBeEI7O0FBQ0EsUUFBTXZFLFVBQVUsR0FBRyxtREFBTyx1REFBVyxLQUFLQSxVQUFoQixFQUE0QnVFLGVBQTVCLENBQVAsRUFBcUQsSUFBckQsQ0FBbkI7O0FBQ0EsUUFBTWhELHVCQUF1QixHQUFHLG1EQUFPLHVEQUFXLEtBQUtBLHVCQUFoQixFQUF5Q2dELGVBQXpDLENBQVAsRUFBa0UsSUFBbEUsQ0FBaEM7O0FBQ0EsUUFBTTVDLGNBQWMsR0FBRyxtREFBTyx1REFBVyxLQUFLQSxjQUFoQixFQUFnQzRDLGVBQWhDLENBQVAsRUFBeUQsSUFBekQsQ0FBdkI7O0FBQ0EsUUFBSS9DLE1BQUosQ0FMYSxDQU9iOztBQUNBeEMsaURBQUMsQ0FBQyxvQkFBRCxFQUF1QixLQUFLRCxZQUE1QixDQUFELENBQTJDMEQsRUFBM0MsQ0FBOEMsT0FBOUMsRUFBdUQsVUFBQUMsS0FBSyxFQUFJO0FBQzVELFVBQU16QyxPQUFPLEdBQUdqQiw2Q0FBQyxDQUFDMEQsS0FBSyxDQUFDOEIsYUFBUCxDQUFqQjtBQUVBOUIsV0FBSyxDQUFDK0IsY0FBTixHQUg0RCxDQUs1RDs7QUFDQXpFLGdCQUFVLENBQUNDLE9BQUQsQ0FBVjtBQUNILEtBUEQsRUFSYSxDQWlCYjs7QUFDQWpCLGlEQUFDLENBQUMsc0JBQUQsRUFBeUIsS0FBS0QsWUFBOUIsQ0FBRCxDQUE2QzBELEVBQTdDLENBQWdELE9BQWhELEVBQXlELFNBQVNpQyxVQUFULEdBQXNCO0FBQzNFbEQsWUFBTSxHQUFHLEtBQUttRCxLQUFkO0FBQ0gsS0FGRCxFQUVHQyxNQUZILENBRVUsVUFBQWxDLEtBQUssRUFBSTtBQUNmLFVBQU16QyxPQUFPLEdBQUdqQiw2Q0FBQyxDQUFDMEQsS0FBSyxDQUFDOEIsYUFBUCxDQUFqQjtBQUNBOUIsV0FBSyxDQUFDK0IsY0FBTixHQUZlLENBSWY7O0FBQ0FsRCw2QkFBdUIsQ0FBQ3RCLE9BQUQsRUFBVXVCLE1BQVYsQ0FBdkI7QUFDSCxLQVJEO0FBVUF4QyxpREFBQyxDQUFDLGNBQUQsRUFBaUIsS0FBS0QsWUFBdEIsQ0FBRCxDQUFxQzBELEVBQXJDLENBQXdDLE9BQXhDLEVBQWlELFVBQUFDLEtBQUssRUFBSTtBQUN0RCxVQUFNbkQsTUFBTSxHQUFHUCw2Q0FBQyxDQUFDMEQsS0FBSyxDQUFDOEIsYUFBUCxDQUFELENBQXVCaEYsSUFBdkIsQ0FBNEIsWUFBNUIsQ0FBZjtBQUNBLFVBQU1xRixNQUFNLEdBQUc3Riw2Q0FBQyxDQUFDMEQsS0FBSyxDQUFDOEIsYUFBUCxDQUFELENBQXVCaEYsSUFBdkIsQ0FBNEIsZUFBNUIsQ0FBZjtBQUNBaUIsaUVBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ05mLFlBQUksRUFBRWtGLE1BREE7QUFFTmxFLFlBQUksRUFBRSxTQUZBO0FBR05tRSx3QkFBZ0IsRUFBRTtBQUhaLE9BQVYsRUFJR0MsSUFKSCxDQUlRLFVBQUMxQixNQUFELEVBQVk7QUFDaEIsWUFBSUEsTUFBTSxDQUFDc0IsS0FBWCxFQUFrQjtBQUNkO0FBQ0FoRCx3QkFBYyxDQUFDcEMsTUFBRCxDQUFkO0FBQ0g7QUFDSixPQVREO0FBVUFtRCxXQUFLLENBQUMrQixjQUFOO0FBQ0gsS0FkRDtBQWdCQXpGLGlEQUFDLENBQUMsa0JBQUQsRUFBcUIsS0FBS0QsWUFBMUIsQ0FBRCxDQUF5QzBELEVBQXpDLENBQTRDLE9BQTVDLEVBQXFELFVBQUFDLEtBQUssRUFBSTtBQUMxRCxVQUFNbkQsTUFBTSxHQUFHUCw2Q0FBQyxDQUFDMEQsS0FBSyxDQUFDOEIsYUFBUCxDQUFELENBQXVCaEYsSUFBdkIsQ0FBNEIsVUFBNUIsQ0FBZjtBQUVBa0QsV0FBSyxDQUFDK0IsY0FBTixHQUgwRCxDQUkxRDs7QUFDQSxZQUFJLENBQUM1QyxlQUFMLENBQXFCdEMsTUFBckI7QUFDSCxLQU5EO0FBT0gsRzs7U0FFRHlGLG1CLEdBQUEsK0JBQXNCO0FBQUE7O0FBQ2xCLFFBQU1DLGdCQUFnQixHQUFHakcsNkNBQUMsQ0FBQyxjQUFELENBQTFCO0FBQ0EsUUFBTWtHLFdBQVcsR0FBR2xHLDZDQUFDLENBQUMsY0FBRCxDQUFyQjtBQUNBLFFBQU1tRyxVQUFVLEdBQUduRyw2Q0FBQyxDQUFDLHFCQUFELEVBQXdCa0csV0FBeEIsQ0FBcEI7QUFFQWxHLGlEQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQnlELEVBQXRCLENBQXlCLE9BQXpCLEVBQWtDLFVBQUFDLEtBQUssRUFBSTtBQUN2Q0EsV0FBSyxDQUFDK0IsY0FBTjtBQUVBekYsbURBQUMsQ0FBQzBELEtBQUssQ0FBQzhCLGFBQVAsQ0FBRCxDQUF1QnBGLElBQXZCO0FBQ0E2RixzQkFBZ0IsQ0FBQ2xGLElBQWpCO0FBQ0FmLG1EQUFDLENBQUMscUJBQUQsQ0FBRCxDQUF5QmUsSUFBekI7QUFDQW9GLGdCQUFVLENBQUM3RyxPQUFYLENBQW1CLE9BQW5CO0FBQ0gsS0FQRDtBQVNBVSxpREFBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJ5RCxFQUF6QixDQUE0QixPQUE1QixFQUFxQyxVQUFBQyxLQUFLLEVBQUk7QUFDMUNBLFdBQUssQ0FBQytCLGNBQU47QUFFQVEsc0JBQWdCLENBQUM3RixJQUFqQjtBQUNBSixtREFBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJJLElBQXpCO0FBQ0FKLG1EQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQmUsSUFBdEI7QUFDSCxLQU5EO0FBUUFtRixlQUFXLENBQUN6QyxFQUFaLENBQWUsUUFBZixFQUF5QixVQUFBQyxLQUFLLEVBQUk7QUFDOUIsVUFBTTBDLElBQUksR0FBR0QsVUFBVSxDQUFDMUYsR0FBWCxFQUFiO0FBRUFpRCxXQUFLLENBQUMrQixjQUFOLEdBSDhCLENBSzlCOztBQUNBLFVBQUksQ0FBQ1csSUFBTCxFQUFXO0FBQ1AsZUFBTzNFLDJEQUFJLENBQUNDLElBQUwsQ0FBVTtBQUNiZixjQUFJLEVBQUV3RixVQUFVLENBQUMzRixJQUFYLENBQWdCLE9BQWhCLENBRE87QUFFYm1CLGNBQUksRUFBRTtBQUZPLFNBQVYsQ0FBUDtBQUlIOztBQUVEQyx3RUFBSyxDQUFDQyxHQUFOLENBQVVDLElBQVYsQ0FBZXVFLFNBQWYsQ0FBeUJELElBQXpCLEVBQStCLFVBQUNwRSxHQUFELEVBQU1DLFFBQU4sRUFBbUI7QUFDOUMsWUFBSUEsUUFBUSxDQUFDekIsSUFBVCxDQUFjMEIsTUFBZCxLQUF5QixTQUE3QixFQUF3QztBQUNwQyxnQkFBSSxDQUFDRSxjQUFMO0FBQ0gsU0FGRCxNQUVPO0FBQ0hYLHFFQUFJLENBQUNDLElBQUwsQ0FBVTtBQUNOZixnQkFBSSxFQUFFc0IsUUFBUSxDQUFDekIsSUFBVCxDQUFjNkIsTUFBZCxDQUFxQkMsSUFBckIsQ0FBMEIsSUFBMUIsQ0FEQTtBQUVOWCxnQkFBSSxFQUFFO0FBRkEsV0FBVjtBQUlIO0FBQ0osT0FURDtBQVVILEtBdkJEO0FBd0JILEc7O1NBRUQyRSx5QixHQUFBLHFDQUE0QjtBQUFBOztBQUN4QixRQUFNQyxjQUFjLEdBQUd2Ryw2Q0FBQyxDQUFDLHdCQUFELENBQXhCO0FBQ0EsUUFBTXdHLFNBQVMsR0FBR3hHLDZDQUFDLENBQUMsNkJBQUQsQ0FBbkI7QUFDQSxRQUFNeUcsVUFBVSxHQUFHekcsNkNBQUMsQ0FBQyxtQkFBRCxFQUFzQndHLFNBQXRCLENBQXBCO0FBRUF4RyxpREFBQyxDQUFDLHVCQUFELENBQUQsQ0FBMkJ5RCxFQUEzQixDQUE4QixPQUE5QixFQUF1QyxVQUFBQyxLQUFLLEVBQUk7QUFDNUNBLFdBQUssQ0FBQytCLGNBQU47QUFDQXpGLG1EQUFDLENBQUMwRCxLQUFLLENBQUM4QixhQUFQLENBQUQsQ0FBdUJrQixNQUF2QjtBQUNBSCxvQkFBYyxDQUFDRyxNQUFmO0FBQ0ExRyxtREFBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEIwRyxNQUE5QjtBQUNILEtBTEQ7QUFPQTFHLGlEQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QnlELEVBQTlCLENBQWlDLE9BQWpDLEVBQTBDLFVBQUFDLEtBQUssRUFBSTtBQUMvQ0EsV0FBSyxDQUFDK0IsY0FBTjtBQUNBYyxvQkFBYyxDQUFDRyxNQUFmO0FBQ0ExRyxtREFBQyxDQUFDLHVCQUFELENBQUQsQ0FBMkIwRyxNQUEzQjtBQUNBMUcsbURBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCMEcsTUFBOUI7QUFDSCxLQUxEO0FBT0FGLGFBQVMsQ0FBQy9DLEVBQVYsQ0FBYSxRQUFiLEVBQXVCLFVBQUFDLEtBQUssRUFBSTtBQUM1QixVQUFNMEMsSUFBSSxHQUFHSyxVQUFVLENBQUNoRyxHQUFYLEVBQWI7QUFFQWlELFdBQUssQ0FBQytCLGNBQU47O0FBRUEsVUFBSSxDQUFDa0Isa0ZBQWEsQ0FBQ1AsSUFBRCxDQUFsQixFQUEwQjtBQUN0QixlQUFPM0UsMkRBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ2JmLGNBQUksRUFBRThGLFVBQVUsQ0FBQ2pHLElBQVgsQ0FBZ0IsT0FBaEIsQ0FETztBQUVibUIsY0FBSSxFQUFFO0FBRk8sU0FBVixDQUFQO0FBSUg7O0FBRURDLHdFQUFLLENBQUNDLEdBQU4sQ0FBVUMsSUFBVixDQUFlOEUsb0JBQWYsQ0FBb0NSLElBQXBDLEVBQTBDLFVBQUNwRSxHQUFELEVBQU02RSxJQUFOLEVBQWU7QUFDckQsWUFBSUEsSUFBSSxDQUFDckcsSUFBTCxDQUFVMEIsTUFBVixLQUFxQixTQUF6QixFQUFvQztBQUNoQyxnQkFBSSxDQUFDRSxjQUFMO0FBQ0gsU0FGRCxNQUVPO0FBQ0hYLHFFQUFJLENBQUNDLElBQUwsQ0FBVTtBQUNOZixnQkFBSSxFQUFFa0csSUFBSSxDQUFDckcsSUFBTCxDQUFVNkIsTUFBVixDQUFpQkMsSUFBakIsQ0FBc0IsSUFBdEIsQ0FEQTtBQUVOWCxnQkFBSSxFQUFFO0FBRkEsV0FBVjtBQUlIO0FBQ0osT0FURDtBQVVILEtBdEJEO0FBdUJILEc7O1NBRURtRixzQixHQUFBLGtDQUF5QjtBQUFBOztBQUNyQixRQUFNaEUsS0FBSyxHQUFHQyxrRUFBWSxFQUExQjtBQUVBL0MsaURBQUMsQ0FBQyxzQkFBRCxDQUFELENBQTBCeUQsRUFBMUIsQ0FBNkIsT0FBN0IsRUFBc0MsVUFBQUMsS0FBSyxFQUFJO0FBQzNDLFVBQU1uRCxNQUFNLEdBQUdQLDZDQUFDLENBQUMwRCxLQUFLLENBQUM4QixhQUFQLENBQUQsQ0FBdUJoRixJQUF2QixDQUE0QixjQUE1QixDQUFmO0FBQ0EsVUFBTXdDLE9BQU8sR0FBRztBQUNaQyxnQkFBUSxFQUFFO0FBREUsT0FBaEI7QUFJQVMsV0FBSyxDQUFDK0IsY0FBTjtBQUVBM0MsV0FBSyxDQUFDSSxJQUFOO0FBRUF0Qix3RUFBSyxDQUFDQyxHQUFOLENBQVVDLElBQVYsQ0FBZWlGLDBCQUFmLENBQTBDeEcsTUFBMUMsRUFBa0R5QyxPQUFsRCxFQUEyRCxVQUFDaEIsR0FBRCxFQUFNQyxRQUFOLEVBQW1CO0FBQzFFYSxhQUFLLENBQUNPLGFBQU4sQ0FBb0JwQixRQUFRLENBQUNxQixPQUE3Qjs7QUFFQSxjQUFJLENBQUNDLG9CQUFMO0FBQ0gsT0FKRDtBQUtILEtBZkQ7QUFnQkgsRzs7U0FFREEsb0IsR0FBQSxnQ0FBdUI7QUFDbkJ2RCxpREFBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEJ5RCxFQUExQixDQUE2QixRQUE3QixFQUF1QyxVQUFBQyxLQUFLLEVBQUk7QUFDNUMsVUFBTXNELE9BQU8sR0FBR2hILDZDQUFDLENBQUMwRCxLQUFLLENBQUM4QixhQUFQLENBQWpCO0FBQ0EsVUFBTXlCLEVBQUUsR0FBR0QsT0FBTyxDQUFDdkcsR0FBUixFQUFYO0FBQ0EsVUFBTXlHLEtBQUssR0FBR0YsT0FBTyxDQUFDeEcsSUFBUixDQUFhLE9BQWIsQ0FBZDs7QUFFQSxVQUFJLENBQUN5RyxFQUFMLEVBQVM7QUFDTDtBQUNIOztBQUVELFVBQU1FLFlBQVksR0FBR0gsT0FBTyxDQUFDSSxJQUFSLG1CQUE2QkgsRUFBN0IsUUFBb0N6RyxJQUFwQyxDQUF5QyxjQUF6QyxDQUFyQjtBQUVBUixtREFBQywwQkFBd0JrSCxLQUF4QixDQUFELENBQWtDOUcsSUFBbEM7QUFDQUosbURBQUMsMEJBQXdCa0gsS0FBeEIsU0FBaUNELEVBQWpDLENBQUQsQ0FBd0NsRyxJQUF4Qzs7QUFFQSxVQUFJb0csWUFBSixFQUFrQjtBQUNkbkgscURBQUMsNEJBQTBCa0gsS0FBMUIsQ0FBRCxDQUFvQ25HLElBQXBDO0FBQ0gsT0FGRCxNQUVPO0FBQ0hmLHFEQUFDLDRCQUEwQmtILEtBQTFCLENBQUQsQ0FBb0M5RyxJQUFwQztBQUNIO0FBQ0osS0FuQkQ7QUFxQkFKLGlEQUFDLENBQUMsc0JBQUQsQ0FBRCxDQUEwQlYsT0FBMUIsQ0FBa0MsUUFBbEM7O0FBRUEsYUFBUytILFdBQVQsR0FBdUI7QUFDbkIsVUFBTTFCLEtBQUssR0FBRzNGLDZDQUFDLENBQUMsMkNBQUQsQ0FBRCxDQUErQ1MsR0FBL0MsRUFBZDtBQUNBLFVBQU02RyxXQUFXLEdBQUd0SCw2Q0FBQyxDQUFDLHNCQUFELENBQXJCO0FBQ0EsVUFBTXVILFVBQVUsR0FBR3ZILDZDQUFDLENBQUMsd0JBQUQsQ0FBcEI7O0FBRUEsVUFBSTJGLEtBQUssS0FBSyxNQUFkLEVBQXNCO0FBQ2xCMkIsbUJBQVcsQ0FBQ3ZHLElBQVo7QUFDQXdHLGtCQUFVLENBQUNuSCxJQUFYO0FBQ0gsT0FIRCxNQUdPO0FBQ0hrSCxtQkFBVyxDQUFDbEgsSUFBWjtBQUNBbUgsa0JBQVUsQ0FBQ3hHLElBQVg7QUFDSDtBQUNKOztBQUVEZixpREFBQyxDQUFDLHVCQUFELENBQUQsQ0FBMkJ5RCxFQUEzQixDQUE4QixPQUE5QixFQUF1QzRELFdBQXZDO0FBRUFBLGVBQVc7QUFDZCxHOztTQUVEaEgsVSxHQUFBLHNCQUFhO0FBQ1QsU0FBS2lGLGNBQUw7QUFDQSxTQUFLVSxtQkFBTDtBQUNBLFNBQUtjLHNCQUFMO0FBQ0EsU0FBS1IseUJBQUwsR0FKUyxDQU1UOztBQUNBLFNBQUtrQixpQkFBTCxHQUF5QixJQUFJQyxnRUFBSixDQUFzQnpILDZDQUFDLENBQUMsMkJBQUQsQ0FBdkIsQ0FBekI7QUFDSCxHOzs7RUEvYjZCMEgscUQ7Ozs7Ozs7Ozs7Ozs7O0FDL0RsQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0lBRXFCRCxpQjtBQUNqQiw2QkFBWUUsUUFBWixFQUFzQjtBQUNsQixTQUFLQSxRQUFMLEdBQWdCQSxRQUFoQjtBQUVBLFNBQUtDLE1BQUwsR0FBYzVILDZDQUFDLENBQUMsMkJBQUQsRUFBOEIsS0FBSzJILFFBQW5DLENBQWY7QUFDQSxTQUFLRSxrQkFBTDtBQUNBLFNBQUtDLHNCQUFMO0FBQ0EsU0FBS0MsbUJBQUw7QUFDSDs7OztTQUVERixrQixHQUFBLDhCQUFxQjtBQUFBOztBQUNqQixTQUFLTCxpQkFBTCxHQUF5QiwrQkFBekI7QUFDQSxTQUFLUSxpQkFBTCxHQUF5QkMsMkRBQUcsQ0FBQztBQUN6QkMsWUFBTSxFQUFLLEtBQUtWLGlCQUFWO0FBRG1CLEtBQUQsQ0FBNUI7QUFJQXhILGlEQUFDLENBQUMsMkJBQUQsRUFBOEIsS0FBSzJILFFBQW5DLENBQUQsQ0FBOENsRSxFQUE5QyxDQUFpRCxPQUFqRCxFQUEwRCxVQUFBQyxLQUFLLEVBQUk7QUFDL0Q7QUFDQTtBQUNBO0FBQ0EsVUFBSTFELDZDQUFDLENBQUksS0FBSSxDQUFDd0gsaUJBQVQsd0NBQUQsQ0FBK0QvRyxHQUEvRCxFQUFKLEVBQTBFO0FBQ3RFLGFBQUksQ0FBQ3VILGlCQUFMLENBQXVCRyxZQUF2QjtBQUNIOztBQUVELFVBQUksS0FBSSxDQUFDSCxpQkFBTCxDQUF1QkksTUFBdkIsQ0FBOEIsT0FBOUIsQ0FBSixFQUE0QztBQUN4QztBQUNIOztBQUVEMUUsV0FBSyxDQUFDK0IsY0FBTjtBQUNILEtBYkQ7QUFlQSxTQUFLNEMsY0FBTDtBQUNBLFNBQUtDLG1CQUFMO0FBQ0EsU0FBS0MsWUFBTDtBQUNILEc7O1NBRURGLGMsR0FBQSwwQkFBaUI7QUFDYixTQUFLTCxpQkFBTCxDQUF1QlEsR0FBdkIsQ0FBMkIsQ0FDdkI7QUFDSUMsY0FBUSxFQUFLLEtBQUtqQixpQkFBVix1Q0FEWjtBQUVJa0IsY0FBUSxFQUFFLGtCQUFDQyxFQUFELEVBQUtsSSxHQUFMLEVBQWE7QUFDbkIsWUFBTW1JLFNBQVMsR0FBR25HLE1BQU0sQ0FBQ2hDLEdBQUQsQ0FBeEI7QUFDQSxZQUFNNEQsTUFBTSxHQUFHdUUsU0FBUyxLQUFLLENBQWQsSUFBbUIsQ0FBQ25HLE1BQU0sQ0FBQ29HLEtBQVAsQ0FBYUQsU0FBYixDQUFuQztBQUVBRCxVQUFFLENBQUN0RSxNQUFELENBQUY7QUFDSCxPQVBMO0FBUUl5RSxrQkFBWSxFQUFFO0FBUmxCLEtBRHVCLENBQTNCO0FBWUgsRzs7U0FFRFIsbUIsR0FBQSwrQkFBc0I7QUFBQTs7QUFDbEIsU0FBS04saUJBQUwsQ0FBdUJRLEdBQXZCLENBQTJCLENBQ3ZCO0FBQ0lDLGNBQVEsRUFBRXpJLDZDQUFDLENBQUksS0FBS3dILGlCQUFULHNDQURmO0FBRUlrQixjQUFRLEVBQUUsa0JBQUNDLEVBQUQsRUFBUTtBQUNkLFlBQUl0RSxNQUFKO0FBRUEsWUFBTTBFLElBQUksR0FBRy9JLDZDQUFDLENBQUksTUFBSSxDQUFDd0gsaUJBQVQsc0NBQWQ7O0FBRUEsWUFBSXVCLElBQUksQ0FBQ2hFLE1BQVQsRUFBaUI7QUFDYixjQUFNaUUsTUFBTSxHQUFHRCxJQUFJLENBQUN0SSxHQUFMLEVBQWY7QUFFQTRELGdCQUFNLEdBQUcyRSxNQUFNLElBQUlBLE1BQU0sQ0FBQ2pFLE1BQWpCLElBQTJCaUUsTUFBTSxLQUFLLGdCQUEvQztBQUNIOztBQUVETCxVQUFFLENBQUN0RSxNQUFELENBQUY7QUFDSCxPQWRMO0FBZUl5RSxrQkFBWSxFQUFFO0FBZmxCLEtBRHVCLENBQTNCO0FBbUJIO0FBRUQ7QUFDSjtBQUNBOzs7U0FDSVAsWSxHQUFBLHdCQUFlO0FBQ1gsUUFBTVUsYUFBYSxHQUFHLCtCQUF0QjtBQUVBakosaURBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVXlELEVBQVYsQ0FBYSxPQUFiLEVBQXNCd0YsYUFBdEIsRUFBcUMsVUFBQ3ZGLEtBQUQsRUFBVztBQUM1QyxVQUFNd0YsaUJBQWlCLEdBQUdsSiw2Q0FBQyxDQUFDLHNCQUFELENBQTNCO0FBQ0EsVUFBTW1KLHFCQUFxQixHQUFHbkosNkNBQUMsQ0FBQywwQkFBRCxDQUEvQjtBQUVBMEQsV0FBSyxDQUFDK0IsY0FBTjtBQUVBeUQsdUJBQWlCLENBQUNFLFdBQWxCLENBQThCLGtCQUE5QjtBQUNBRCwyQkFBcUIsQ0FBQ0MsV0FBdEIsQ0FBa0Msa0JBQWxDO0FBQ0gsS0FSRDtBQVNILEc7O1NBRUR0QixzQixHQUFBLGtDQUF5QjtBQUFBOztBQUNyQixRQUFJdUIsS0FBSixDQURxQixDQUdyQjs7QUFDQUMseUVBQVksQ0FBQyxLQUFLMUIsTUFBTixFQUFjLEtBQUsyQixPQUFuQixFQUE0QjtBQUFFQyxvQkFBYyxFQUFFO0FBQWxCLEtBQTVCLEVBQXNELFVBQUN4SCxHQUFELEVBQU15SCxLQUFOLEVBQWdCO0FBQzlFLFVBQUl6SCxHQUFKLEVBQVM7QUFDTFAsbUVBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ05mLGNBQUksRUFBRXFCLEdBREE7QUFFTkwsY0FBSSxFQUFFO0FBRkEsU0FBVjtBQUtBLGNBQU0sSUFBSStILEtBQUosQ0FBVTFILEdBQVYsQ0FBTjtBQUNIOztBQUVELFVBQU0ySCxNQUFNLEdBQUczSiw2Q0FBQyxDQUFDeUosS0FBRCxDQUFoQjs7QUFFQSxVQUFJLE1BQUksQ0FBQ3pCLGlCQUFMLENBQXVCNEIsU0FBdkIsQ0FBaUMsTUFBSSxDQUFDaEMsTUFBdEMsTUFBa0QsV0FBdEQsRUFBbUU7QUFDL0QsY0FBSSxDQUFDSSxpQkFBTCxDQUF1QjdGLE1BQXZCLENBQThCLE1BQUksQ0FBQ3lGLE1BQW5DO0FBQ0g7O0FBRUQsVUFBSXlCLEtBQUosRUFBVztBQUNQLGNBQUksQ0FBQ3JCLGlCQUFMLENBQXVCN0YsTUFBdkIsQ0FBOEJrSCxLQUE5QjtBQUNIOztBQUVELFVBQUlNLE1BQU0sQ0FBQ0UsRUFBUCxDQUFVLFFBQVYsQ0FBSixFQUF5QjtBQUNyQlIsYUFBSyxHQUFHSSxLQUFSOztBQUNBLGNBQUksQ0FBQ25CLG1CQUFMO0FBQ0gsT0FIRCxNQUdPO0FBQ0hxQixjQUFNLENBQUN6RixJQUFQLENBQVksYUFBWixFQUEyQixnQkFBM0I7QUFDQTRGLHFFQUFVLENBQUNDLHNCQUFYLENBQWtDTixLQUFsQztBQUNILE9BMUI2RSxDQTRCOUU7QUFDQTtBQUNBOzs7QUFDQXpKLG1EQUFDLENBQUMsTUFBSSxDQUFDd0gsaUJBQU4sQ0FBRCxDQUEwQkosSUFBMUIsQ0FBK0Isc0JBQS9CLEVBQXVENEMsV0FBdkQsQ0FBbUUscUJBQW5FO0FBQ0gsS0FoQ1csQ0FBWjtBQWlDSCxHOztTQUVEakMsbUIsR0FBQSwrQkFBc0I7QUFDbEIsUUFBTWtDLG1CQUFtQixHQUFHakssNkNBQUMsQ0FBQyxxQkFBRCxDQUE3QjtBQUNBLFFBQU1rSyxjQUFjLEdBQUdsSyw2Q0FBQyxDQUFDLGlCQUFELENBQXhCO0FBRUFrSyxrQkFBYyxDQUFDekcsRUFBZixDQUFrQixRQUFsQixFQUE0QixVQUFBQyxLQUFLLEVBQUk7QUFDakMsVUFBTXlHLE1BQU0sR0FBRztBQUNYQyxrQkFBVSxFQUFFcEssNkNBQUMsQ0FBQywyQkFBRCxFQUE4QmtLLGNBQTlCLENBQUQsQ0FBK0N6SixHQUEvQyxFQUREO0FBRVg0SixnQkFBUSxFQUFFckssNkNBQUMsQ0FBQyx5QkFBRCxFQUE0QmtLLGNBQTVCLENBQUQsQ0FBNkN6SixHQUE3QyxFQUZDO0FBR1g2SixZQUFJLEVBQUV0Syw2Q0FBQyxDQUFDLHdCQUFELEVBQTJCa0ssY0FBM0IsQ0FBRCxDQUE0Q3pKLEdBQTVDLEVBSEs7QUFJWDhKLGdCQUFRLEVBQUV2Syw2Q0FBQyxDQUFDLHVCQUFELEVBQTBCa0ssY0FBMUIsQ0FBRCxDQUEyQ3pKLEdBQTNDO0FBSkMsT0FBZjtBQU9BaUQsV0FBSyxDQUFDK0IsY0FBTjtBQUVBN0Qsd0VBQUssQ0FBQ0MsR0FBTixDQUFVQyxJQUFWLENBQWUwSSxpQkFBZixDQUFpQ0wsTUFBakMsRUFBeUMsc0JBQXpDLEVBQWlFLFVBQUNuSSxHQUFELEVBQU1DLFFBQU4sRUFBbUI7QUFDaEZqQyxxREFBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0JvRixJQUF0QixDQUEyQm5ELFFBQVEsQ0FBQ3FCLE9BQXBDLEVBRGdGLENBR2hGOztBQUNBdEQscURBQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCeUQsRUFBNUIsQ0FBK0IsT0FBL0IsRUFBd0MsVUFBQWdILFVBQVUsRUFBSTtBQUNsRCxjQUFNQyxPQUFPLEdBQUcxSyw2Q0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJTLEdBQTdCLEVBQWhCO0FBRUFnSyxvQkFBVSxDQUFDaEYsY0FBWDtBQUVBN0QsNEVBQUssQ0FBQ0MsR0FBTixDQUFVQyxJQUFWLENBQWU2SSxtQkFBZixDQUFtQ0QsT0FBbkMsRUFBNEMsWUFBTTtBQUM5QzFGLGtCQUFNLENBQUNDLFFBQVAsQ0FBZ0JDLE1BQWhCO0FBQ0gsV0FGRDtBQUdILFNBUkQ7QUFTSCxPQWJEO0FBY0gsS0F4QkQ7QUEwQkFsRixpREFBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJ5RCxFQUE3QixDQUFnQyxPQUFoQyxFQUF5QyxVQUFBQyxLQUFLLEVBQUk7QUFDOUNBLFdBQUssQ0FBQytCLGNBQU47QUFFQXpGLG1EQUFDLENBQUMwRCxLQUFLLENBQUM4QixhQUFQLENBQUQsQ0FBdUJwRixJQUF2QjtBQUNBNkoseUJBQW1CLENBQUNELFdBQXBCLENBQWdDLGtCQUFoQztBQUNBaEssbURBQUMsQ0FBQyx5QkFBRCxDQUFELENBQTZCZSxJQUE3QjtBQUNILEtBTkQ7QUFTQWYsaURBQUMsQ0FBQyx5QkFBRCxDQUFELENBQTZCeUQsRUFBN0IsQ0FBZ0MsT0FBaEMsRUFBeUMsVUFBQUMsS0FBSyxFQUFJO0FBQzlDQSxXQUFLLENBQUMrQixjQUFOO0FBRUF3RSx5QkFBbUIsQ0FBQ1csUUFBcEIsQ0FBNkIsa0JBQTdCO0FBQ0E1SyxtREFBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJlLElBQTdCO0FBQ0FmLG1EQUFDLENBQUMseUJBQUQsQ0FBRCxDQUE2QkksSUFBN0I7QUFDSCxLQU5EO0FBT0gsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0TEw7QUFBZSx5RUFBVXlLLElBQVYsRUFBZ0I7QUFDM0IsTUFBSSxPQUFPQSxJQUFQLEtBQWdCLFFBQXBCLEVBQThCO0FBQzFCLFdBQU8sS0FBUDtBQUNILEdBSDBCLENBSzNCOzs7QUFDQSxTQUFPLElBQVA7QUFDSCxDIiwiZmlsZSI6InRoZW1lLWJ1bmRsZS5jaHVuay4xMC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQYWdlTWFuYWdlciBmcm9tICcuL3BhZ2UtbWFuYWdlcic7XG5pbXBvcnQgJCBmcm9tICdqcXVlcnknO1xuaW1wb3J0IF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCBnaWZ0Q2VydENoZWNrIGZyb20gJy4vY29tbW9uL2dpZnQtY2VydGlmaWNhdGUtdmFsaWRhdG9yJztcbmltcG9ydCB1dGlscyBmcm9tICdAYmlnY29tbWVyY2Uvc3RlbmNpbC11dGlscyc7XG5pbXBvcnQgU2hpcHBpbmdFc3RpbWF0b3IgZnJvbSAnLi9jYXJ0L3NoaXBwaW5nLWVzdGltYXRvcic7XG5pbXBvcnQgeyBkZWZhdWx0TW9kYWwgfSBmcm9tICcuL2dsb2JhbC9tb2RhbCc7XG5pbXBvcnQgc3dhbCBmcm9tICcuL2dsb2JhbC9zd2VldC1hbGVydCc7XG5cbmZ1bmN0aW9uIHNldFNoaXBwaW5nTWVzc2FnZShpbnYsIHF1YW50aXR5LCBpc0RlZmF1bHQpIHtcbiAgICB2YXIgbGVhZHRpbWUgPSA1O1xuICAgIHZhciB0cmlnZ2VyID0gMjU7XG4gICAgdmFyIG5iZE1zZyA9IFwiIFNjaGVkdWxlZCB0byBzaGlwIG5leHQgYnVzaW5lc3MgZGF5XCI7XG4gICAgdmFyIGxlYWR0aW1lTXNnID0gXCIgU2NoZWR1bGVkIHRvIHNoaXAgaW4gXCIgKyAoMSArIGxlYWR0aW1lKSArIFwiIGJ1c2luZXNzIGRheXNcIjtcbiAgICB2YXIgYXZhaWxhYmxlID0gaW52O1xuXG4gICAgdmFyIHF0eSA9IHF1YW50aXR5O1xuICAgIHZhciBkaWZmID0gYXZhaWxhYmxlIC0gcXR5O1xuICAgIHZhciBtZXNzYWdlID0gJyc7XG5cbiAgICBpZiAoaXNEZWZhdWx0KSB7XG4gICAgICAgIC8vcXVhbnRpdHkgaXMgMSBieSBkZWZhdWx0XG4gICAgICAgIGlmIChhdmFpbGFibGUgPiAwICYmIHF1YW50aXR5IDw9IGF2YWlsYWJsZSApIHtcbiAgICAgICAgICAgIG1lc3NhZ2UgPSAnU2hpcHMgTmV4dCBCdXNpbmVzcyBEYXknO1xuICAgICAgICB9IGVsc2UgaWYgKGF2YWlsYWJsZSA8PSAwICYmIGRpZmYgPiAtMjUpIHtcbiAgICAgICAgICAgIG1lc3NhZ2UgPSAnU2hpcHMgaW4gJyAgKyAoMSArIGxlYWR0aW1lKSArICcgYnVzaW5lc3MgZGF5cyc7XG4gICAgICAgIH0gZWxzZSBpZiAoYXZhaWxhYmxlIDw9IDAgJiYgZGlmZiA8PSAtMjUpIHtcbiAgICAgICAgICAgIG1lc3NhZ2UgPSAnUGxlYXNlIGNvbnRhY3QgdXMgZm9yIGEgc2hpcCBkYXRlJztcbiAgICAgICAgfVxuXG4gICAgfSBlbHNlIHtcblxuICAgICAgICBpZiAoZGlmZiA+PSAwICkge1xuICAgICAgICAgICAgaWYgKGRpZmYgPCBhdmFpbGFibGUgJiYgYXZhaWxhYmxlID4gMCkge1xuICAgICAgICAgICAgICAgIG1lc3NhZ2UgPSBxdHkgKyBuYmRNc2c7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfSBlbHNlICB7XG4gICAgICAgICAgICBpZiAoZGlmZiA8IHRyaWdnZXIgICYmIGF2YWlsYWJsZSA+IDAgKSB7XG4gICAgICAgICAgICAgICAgaWYgKGRpZmYgPD0gLXRyaWdnZXIpIHtcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZSA9ICdQbGVhc2UgY29udGFjdCB1cyBmb3IgYSBzaGlwIGRhdGUnO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2UgPSBhdmFpbGFibGUgKyBuYmRNc2cgK1wiOyBcIisgLWRpZmYgKyBsZWFkdGltZU1zZztcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH0gZWxzZSBpZiAoLWRpZmYgPCB0cmlnZ2VyICAmJiBhdmFpbGFibGUgPD0gMCApICB7XG4gICAgICAgICAgICAgICAgbWVzc2FnZSA9IGxlYWR0aW1lTXNnO1xuXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIG1lc3NhZ2UgPSAnUGxlYXNlIGNvbnRhY3QgdXMgZm9yIGEgc2hpcCBkYXRlJztcblxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICB9XG5cbiAgICAvL2NvbnNvbGUubG9nKCBcIiAgRGVsdGEgaXM6ICBcIiArIGRpZmYgKyBcIiAsIFF0eSBSZXF1ZXN0ZWQ6IFwiICsgcXVhbnRpdHkgK1wiICwgUXR5IE9uIEhhbmQ6IFwiICsgYXZhaWxhYmxlKTtcblxuICAgIHJldHVybiBtZXNzYWdlO1xuXG59XG5cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2FydCBleHRlbmRzIFBhZ2VNYW5hZ2VyIHtcbiAgICBvblJlYWR5KCkge1xuICAgICAgICB0aGlzLiRjYXJ0Q29udGVudCA9ICQoJ1tkYXRhLWNhcnQtY29udGVudF0nKTtcbiAgICAgICAgdGhpcy4kY2FydE1lc3NhZ2VzID0gJCgnW2RhdGEtY2FydC1zdGF0dXNdJyk7XG4gICAgICAgIHRoaXMuJGNhcnRUb3RhbHMgPSAkKCdbZGF0YS1jYXJ0LXRvdGFsc10nKTtcbiAgICAgICAgdGhpcy4kb3ZlcmxheSA9ICQoJ1tkYXRhLWNhcnRdIC5sb2FkaW5nT3ZlcmxheScpXG4gICAgICAgICAgICAuaGlkZSgpOyAvLyBUT0RPOiB0ZW1wb3JhcnkgdW50aWwgcm9wZXIgcHVsbHMgaW4gaGlzIGNhcnQgY29tcG9uZW50c1xuXG4gICAgICAgIHRoaXMuYmluZEV2ZW50cygpO1xuXG4gICAgICAkKCcuY2FydC1pdGVtLXF0eS1pbnB1dCcpLmVhY2goIGZ1bmN0aW9uICgpe1xuICAgICAgICAgIHZhciBpdGVtSWQgPSAkKHRoaXMpLmRhdGEoJ2NhcnRJdGVtaWQnKTtcbiAgICAgICAgICB2YXIgcXR5ID0gJChgI3F0eS0ke2l0ZW1JZH1gKS52YWwoKTtcblxuICAgICAgICAgIHZhciBpbnYgPSBwYXJzZUludCgkKGAjc3Njb3ItaW52ZW50b3J5LSR7aXRlbUlkfWApLnRleHQoKSk7XG5cbiAgICAgICAgICB2YXIgbXNnID0gc2V0U2hpcHBpbmdNZXNzYWdlKGludiwgcXR5LCBmYWxzZSk7XG4gICAgICAgICAgY29uc29sZS5sb2cobXNnICsgXCIgb24gbG9hZFwiKTtcbiAgICAgICAgICAkKGAjc3Njb3ItYXZhaWxhYmlsaXR5LSR7aXRlbUlkfWApLnRleHQobXNnKTtcbiAgICAgICAgICAkKGAjc3Njb3ItYXZhaWxhYmlsaXR5LSR7aXRlbUlkfWApLnNob3coKTtcbiAgICAgICB9XG4gICAgICApO1xuXG4gICAgfVxuXG4gICAgY2FydFVwZGF0ZSgkdGFyZ2V0KSB7XG4gICAgICAgIGNvbnN0IGl0ZW1JZCA9ICR0YXJnZXQuZGF0YSgnY2FydEl0ZW1pZCcpO1xuICAgICAgICBjb25zdCAkZWwgPSAkKGAjcXR5LSR7aXRlbUlkfWApO1xuICAgICAgICBjb25zdCBvbGRRdHkgPSBwYXJzZUludCgkZWwudmFsKCksIDEwKTtcbiAgICAgICAgY29uc3QgbWF4UXR5ID0gcGFyc2VJbnQoJGVsLmRhdGEoJ3F1YW50aXR5TWF4JyksIDEwKTtcbiAgICAgICAgY29uc3QgbWluUXR5ID0gcGFyc2VJbnQoJGVsLmRhdGEoJ3F1YW50aXR5TWluJyksIDEwKTtcbiAgICAgICAgY29uc3QgbWluRXJyb3IgPSAkZWwuZGF0YSgncXVhbnRpdHlNaW5FcnJvcicpO1xuICAgICAgICBjb25zdCBtYXhFcnJvciA9ICRlbC5kYXRhKCdxdWFudGl0eU1heEVycm9yJyk7XG4gICAgICAgIGNvbnN0IG5ld1F0eSA9ICR0YXJnZXQuZGF0YSgnYWN0aW9uJykgPT09ICdpbmMnID8gb2xkUXR5ICsgMSA6IG9sZFF0eSAtIDE7XG4gICAgICAgIC8vIERvZXMgbm90IHF1YWxpdHkgZm9yIG1pbi9tYXggcXVhbnRpdHlcbiAgICAgICAgaWYgKG5ld1F0eSA8IG1pblF0eSkge1xuICAgICAgICAgICAgcmV0dXJuIHN3YWwuZmlyZSh7XG4gICAgICAgICAgICAgICAgdGV4dDogbWluRXJyb3IsXG4gICAgICAgICAgICAgICAgaWNvbjogJ2Vycm9yJyxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2UgaWYgKG1heFF0eSA+IDAgJiYgbmV3UXR5ID4gbWF4UXR5KSB7XG4gICAgICAgICAgICByZXR1cm4gc3dhbC5maXJlKHtcbiAgICAgICAgICAgICAgICB0ZXh0OiBtYXhFcnJvcixcbiAgICAgICAgICAgICAgICBpY29uOiAnZXJyb3InLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLiRvdmVybGF5LnNob3coKTtcblxuICAgICAgICB1dGlscy5hcGkuY2FydC5pdGVtVXBkYXRlKGl0ZW1JZCwgbmV3UXR5LCAoZXJyLCByZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgdGhpcy4kb3ZlcmxheS5oaWRlKCk7XG5cbiAgICAgICAgICAgIGlmIChyZXNwb25zZS5kYXRhLnN0YXR1cyA9PT0gJ3N1Y2NlZWQnKSB7XG4gICAgICAgICAgICAgICAgLy8gaWYgdGhlIHF1YW50aXR5IGlzIGNoYW5nZWQgXCIxXCIgZnJvbSBcIjBcIiwgd2UgaGF2ZSB0byByZW1vdmUgdGhlIHJvdy5cbiAgICAgICAgICAgICAgICBjb25zdCByZW1vdmUgPSAobmV3UXR5ID09PSAwKTtcblxuICAgICAgICAgICAgICAgIHRoaXMucmVmcmVzaENvbnRlbnQocmVtb3ZlKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJGVsLnZhbChvbGRRdHkpO1xuICAgICAgICAgICAgICAgIHN3YWwuZmlyZSh7XG4gICAgICAgICAgICAgICAgICAgIHRleHQ6IHJlc3BvbnNlLmRhdGEuZXJyb3JzLmpvaW4oJ1xcbicpLFxuICAgICAgICAgICAgICAgICAgICBpY29uOiAnZXJyb3InLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBjYXJ0VXBkYXRlUXR5VGV4dENoYW5nZSgkdGFyZ2V0LCBwcmVWYWwgPSBudWxsKSB7XG4gICAgICAgIGNvbnN0IGl0ZW1JZCA9ICR0YXJnZXQuZGF0YSgnY2FydEl0ZW1pZCcpO1xuICAgICAgICBjb25zdCAkZWwgPSAkKGAjcXR5LSR7aXRlbUlkfWApO1xuICAgICAgICBjb25zdCBtYXhRdHkgPSBwYXJzZUludCgkZWwuZGF0YSgncXVhbnRpdHlNYXgnKSwgMTApO1xuICAgICAgICBjb25zdCBtaW5RdHkgPSBwYXJzZUludCgkZWwuZGF0YSgncXVhbnRpdHlNaW4nKSwgMTApO1xuICAgICAgICBjb25zdCBvbGRRdHkgPSBwcmVWYWwgIT09IG51bGwgPyBwcmVWYWwgOiBtaW5RdHk7XG4gICAgICAgIGNvbnN0IG1pbkVycm9yID0gJGVsLmRhdGEoJ3F1YW50aXR5TWluRXJyb3InKTtcbiAgICAgICAgY29uc3QgbWF4RXJyb3IgPSAkZWwuZGF0YSgncXVhbnRpdHlNYXhFcnJvcicpO1xuICAgICAgICBjb25zdCBuZXdRdHkgPSBwYXJzZUludChOdW1iZXIoJGVsLnZhbCgpKSwgMTApO1xuICAgICAgICBsZXQgaW52YWxpZEVudHJ5O1xuXG4gICAgICAgIC8vIERvZXMgbm90IHF1YWxpdHkgZm9yIG1pbi9tYXggcXVhbnRpdHlcbiAgICAgICAgaWYgKCFuZXdRdHkpIHtcbiAgICAgICAgICAgIGludmFsaWRFbnRyeSA9ICRlbC52YWwoKTtcbiAgICAgICAgICAgICRlbC52YWwob2xkUXR5KTtcbiAgICAgICAgICAgIHJldHVybiBzd2FsLmZpcmUoe1xuICAgICAgICAgICAgICAgIHRleHQ6IGAke2ludmFsaWRFbnRyeX0gaXMgbm90IGEgdmFsaWQgZW50cnlgLFxuICAgICAgICAgICAgICAgIGljb246ICdlcnJvcicsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIGlmIChuZXdRdHkgPCBtaW5RdHkpIHtcbiAgICAgICAgICAgICRlbC52YWwob2xkUXR5KTtcbiAgICAgICAgICAgIHJldHVybiBzd2FsLmZpcmUoe1xuICAgICAgICAgICAgICAgIHRleHQ6IG1pbkVycm9yLFxuICAgICAgICAgICAgICAgIGljb246ICdlcnJvcicsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIGlmIChtYXhRdHkgPiAwICYmIG5ld1F0eSA+IG1heFF0eSkge1xuICAgICAgICAgICAgJGVsLnZhbChvbGRRdHkpO1xuICAgICAgICAgICAgcmV0dXJuIHN3YWwuZmlyZSh7XG4gICAgICAgICAgICAgICAgdGV4dDogbWF4RXJyb3IsXG4gICAgICAgICAgICAgICAgaWNvbjogJ2Vycm9yJyxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy4kb3ZlcmxheS5zaG93KCk7XG4gICAgICAgIHV0aWxzLmFwaS5jYXJ0Lml0ZW1VcGRhdGUoaXRlbUlkLCBuZXdRdHksIChlcnIsIHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLiRvdmVybGF5LmhpZGUoKTtcblxuICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmRhdGEuc3RhdHVzID09PSAnc3VjY2VlZCcpIHtcbiAgICAgICAgICAgICAgICAvLyBpZiB0aGUgcXVhbnRpdHkgaXMgY2hhbmdlZCBcIjFcIiBmcm9tIFwiMFwiLCB3ZSBoYXZlIHRvIHJlbW92ZSB0aGUgcm93LlxuICAgICAgICAgICAgICAgIGNvbnN0IHJlbW92ZSA9IChuZXdRdHkgPT09IDApO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5yZWZyZXNoQ29udGVudChyZW1vdmUpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkZWwudmFsKG9sZFF0eSk7XG4gICAgICAgICAgICAgICAgc3dhbC5maXJlKHtcbiAgICAgICAgICAgICAgICAgICAgdGV4dDogcmVzcG9uc2UuZGF0YS5lcnJvcnMuam9pbignXFxuJyksXG4gICAgICAgICAgICAgICAgICAgIGljb246ICdlcnJvcicsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGNhcnRSZW1vdmVJdGVtKGl0ZW1JZCkge1xuICAgICAgICB0aGlzLiRvdmVybGF5LnNob3coKTtcbiAgICAgICAgdXRpbHMuYXBpLmNhcnQuaXRlbVJlbW92ZShpdGVtSWQsIChlcnIsIHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBpZiAocmVzcG9uc2UuZGF0YS5zdGF0dXMgPT09ICdzdWNjZWVkJykge1xuICAgICAgICAgICAgICAgIHRoaXMucmVmcmVzaENvbnRlbnQodHJ1ZSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHN3YWwuZmlyZSh7XG4gICAgICAgICAgICAgICAgICAgIHRleHQ6IHJlc3BvbnNlLmRhdGEuZXJyb3JzLmpvaW4oJ1xcbicpLFxuICAgICAgICAgICAgICAgICAgICBpY29uOiAnZXJyb3InLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBjYXJ0RWRpdE9wdGlvbnMoaXRlbUlkKSB7XG4gICAgICAgIGNvbnN0IG1vZGFsID0gZGVmYXVsdE1vZGFsKCk7XG4gICAgICAgIGNvbnN0IG9wdGlvbnMgPSB7XG4gICAgICAgICAgICB0ZW1wbGF0ZTogJ2NhcnQvbW9kYWxzL2NvbmZpZ3VyZS1wcm9kdWN0JyxcbiAgICAgICAgfTtcblxuICAgICAgICBtb2RhbC5vcGVuKCk7XG5cbiAgICAgICAgdXRpbHMuYXBpLnByb2R1Y3RBdHRyaWJ1dGVzLmNvbmZpZ3VyZUluQ2FydChpdGVtSWQsIG9wdGlvbnMsIChlcnIsIHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBtb2RhbC51cGRhdGVDb250ZW50KHJlc3BvbnNlLmNvbnRlbnQpO1xuXG4gICAgICAgICAgICB0aGlzLmJpbmRHaWZ0V3JhcHBpbmdGb3JtKCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHV0aWxzLmhvb2tzLm9uKCdwcm9kdWN0LW9wdGlvbi1jaGFuZ2UnLCAoZXZlbnQsIG9wdGlvbikgPT4ge1xuICAgICAgICAgICAgY29uc3QgJGNoYW5nZWRPcHRpb24gPSAkKG9wdGlvbik7XG4gICAgICAgICAgICBjb25zdCAkZm9ybSA9ICRjaGFuZ2VkT3B0aW9uLnBhcmVudHMoJ2Zvcm0nKTtcbiAgICAgICAgICAgIGNvbnN0ICRzdWJtaXQgPSAkKCdpbnB1dC5idXR0b24nLCAkZm9ybSk7XG4gICAgICAgICAgICBjb25zdCAkbWVzc2FnZUJveCA9ICQoJy5hbGVydE1lc3NhZ2VCb3gnKTtcbiAgICAgICAgICAgIGNvbnN0IGl0ZW0gPSAkKCdbbmFtZT1cIml0ZW1faWRcIl0nLCAkZm9ybSkuYXR0cigndmFsdWUnKTtcblxuICAgICAgICAgICAgdXRpbHMuYXBpLnByb2R1Y3RBdHRyaWJ1dGVzLm9wdGlvbkNoYW5nZShpdGVtLCAkZm9ybS5zZXJpYWxpemUoKSwgKGVyciwgcmVzdWx0KSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgZGF0YSA9IHJlc3VsdC5kYXRhIHx8IHt9O1xuXG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgICAgICAgICAgICBzd2FsLmZpcmUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogZXJyLFxuICAgICAgICAgICAgICAgICAgICAgICAgaWNvbjogJ2Vycm9yJyxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoZGF0YS5wdXJjaGFzaW5nX21lc3NhZ2UpIHtcbiAgICAgICAgICAgICAgICAgICAgJCgncC5hbGVydEJveC1tZXNzYWdlJywgJG1lc3NhZ2VCb3gpLnRleHQoZGF0YS5wdXJjaGFzaW5nX21lc3NhZ2UpO1xuICAgICAgICAgICAgICAgICAgICAkc3VibWl0LnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgICAgICRtZXNzYWdlQm94LnNob3coKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAkc3VibWl0LnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICAkbWVzc2FnZUJveC5oaWRlKCk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKCFkYXRhLnB1cmNoYXNhYmxlIHx8ICFkYXRhLmluc3RvY2spIHtcbiAgICAgICAgICAgICAgICAgICAgJHN1Ym1pdC5wcm9wKCdkaXNhYmxlZCcsIHRydWUpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICRzdWJtaXQucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHJlZnJlc2hDb250ZW50KHJlbW92ZSkge1xuICAgICAgICBjb25zdCAkY2FydEl0ZW1zUm93cyA9ICQoJ1tkYXRhLWl0ZW0tcm93XScsIHRoaXMuJGNhcnRDb250ZW50KTtcbiAgICAgICAgY29uc3QgJGNhcnRQYWdlVGl0bGUgPSAkKCdbZGF0YS1jYXJ0LXBhZ2UtdGl0bGVdJyk7XG4gICAgICAgIGNvbnN0IG9wdGlvbnMgPSB7XG4gICAgICAgICAgICB0ZW1wbGF0ZToge1xuICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICdjYXJ0L2NvbnRlbnQnLFxuICAgICAgICAgICAgICAgIHRvdGFsczogJ2NhcnQvdG90YWxzJyxcbiAgICAgICAgICAgICAgICBwYWdlVGl0bGU6ICdjYXJ0L3BhZ2UtdGl0bGUnLFxuICAgICAgICAgICAgICAgIHN0YXR1c01lc3NhZ2VzOiAnY2FydC9zdGF0dXMtbWVzc2FnZXMnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgfTtcblxuICAgICAgICB0aGlzLiRvdmVybGF5LnNob3coKTtcblxuICAgICAgICAvLyBSZW1vdmUgbGFzdCBpdGVtIGZyb20gY2FydD8gUmVsb2FkXG4gICAgICAgIGlmIChyZW1vdmUgJiYgJGNhcnRJdGVtc1Jvd3MubGVuZ3RoID09PSAxKSB7XG4gICAgICAgICAgICByZXR1cm4gd2luZG93LmxvY2F0aW9uLnJlbG9hZCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgdXRpbHMuYXBpLmNhcnQuZ2V0Q29udGVudChvcHRpb25zLCAoZXJyLCByZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgdGhpcy4kY2FydENvbnRlbnQuaHRtbChyZXNwb25zZS5jb250ZW50KTtcbiAgICAgICAgICAgIHRoaXMuJGNhcnRUb3RhbHMuaHRtbChyZXNwb25zZS50b3RhbHMpO1xuICAgICAgICAgICAgdGhpcy4kY2FydE1lc3NhZ2VzLmh0bWwocmVzcG9uc2Uuc3RhdHVzTWVzc2FnZXMpO1xuXG4gICAgICAgICAgICAkY2FydFBhZ2VUaXRsZS5yZXBsYWNlV2l0aChyZXNwb25zZS5wYWdlVGl0bGUpO1xuICAgICAgICAgICAgdGhpcy5iaW5kRXZlbnRzKCk7XG4gICAgICAgICAgICB0aGlzLiRvdmVybGF5LmhpZGUoKTtcblxuICAgICAgICAgICAgY29uc3QgcXVhbnRpdHkgPSAkKCdbZGF0YS1jYXJ0LXF1YW50aXR5XScsIHRoaXMuJGNhcnRDb250ZW50KS5kYXRhKCdjYXJ0UXVhbnRpdHknKSB8fCAwO1xuXG4gICAgICAgICAgICAkKCdib2R5JykudHJpZ2dlcignY2FydC1xdWFudGl0eS11cGRhdGUnLCBxdWFudGl0eSk7XG5cbiAgICAgICAgICAgIC8vU1NDT1JcbiAgICAgICAgICAgICQoJy5jYXJ0LWl0ZW0tcXR5LWlucHV0JykuZWFjaCggZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHZhciBpdGVtSWQgPSAkKHRoaXMpLmRhdGEoJ2NhcnRJdGVtaWQnKTtcbiAgICAgICAgICAgICAgICB2YXIgaW52ID0gcGFyc2VJbnQoJChgI3NzY29yLWludmVudG9yeS0ke2l0ZW1JZH1gKS50ZXh0KCkpO1xuICAgICAgICAgICAgICAgIHZhciBxdHkgPSAkKGAjcXR5LSR7aXRlbUlkfWApLnZhbCgpO1xuICAgICAgICAgICAgICAgIHZhciBtc2cgPSBzZXRTaGlwcGluZ01lc3NhZ2UoaW52LCBxdHksIGZhbHNlKTtcbiAgICAgICAgICAgICAvLyAgIGNvbnNvbGUubG9nKG1zZyArIFwiIG9uIGNoYW5nZVwiKTtcbiAgICAgICAgICAgICAgICAkKGAjc3Njb3ItYXZhaWxhYmlsaXR5LSR7aXRlbUlkfWApLnRleHQobXNnKTtcbiAgICAgICAgICAgICAgICAkKGAjc3Njb3ItYXZhaWxhYmlsaXR5LSR7aXRlbUlkfWApLnNob3coKTtcblxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYmluZENhcnRFdmVudHMoKSB7XG4gICAgICAgIGNvbnN0IGRlYm91bmNlVGltZW91dCA9IDQwMDtcbiAgICAgICAgY29uc3QgY2FydFVwZGF0ZSA9IF8uYmluZChfLmRlYm91bmNlKHRoaXMuY2FydFVwZGF0ZSwgZGVib3VuY2VUaW1lb3V0KSwgdGhpcyk7XG4gICAgICAgIGNvbnN0IGNhcnRVcGRhdGVRdHlUZXh0Q2hhbmdlID0gXy5iaW5kKF8uZGVib3VuY2UodGhpcy5jYXJ0VXBkYXRlUXR5VGV4dENoYW5nZSwgZGVib3VuY2VUaW1lb3V0KSwgdGhpcyk7XG4gICAgICAgIGNvbnN0IGNhcnRSZW1vdmVJdGVtID0gXy5iaW5kKF8uZGVib3VuY2UodGhpcy5jYXJ0UmVtb3ZlSXRlbSwgZGVib3VuY2VUaW1lb3V0KSwgdGhpcyk7XG4gICAgICAgIGxldCBwcmVWYWw7XG5cbiAgICAgICAgLy8gY2FydCB1cGRhdGVcbiAgICAgICAgJCgnW2RhdGEtY2FydC11cGRhdGVdJywgdGhpcy4kY2FydENvbnRlbnQpLm9uKCdjbGljaycsIGV2ZW50ID0+IHtcbiAgICAgICAgICAgIGNvbnN0ICR0YXJnZXQgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xuXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICAvLyB1cGRhdGUgY2FydCBxdWFudGl0eVxuICAgICAgICAgICAgY2FydFVwZGF0ZSgkdGFyZ2V0KTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gY2FydCBxdHkgbWFudWFsbHkgdXBkYXRlc1xuICAgICAgICAkKCcuY2FydC1pdGVtLXF0eS1pbnB1dCcsIHRoaXMuJGNhcnRDb250ZW50KS5vbignZm9jdXMnLCBmdW5jdGlvbiBvblF0eUZvY3VzKCkge1xuICAgICAgICAgICAgcHJlVmFsID0gdGhpcy52YWx1ZTtcbiAgICAgICAgfSkuY2hhbmdlKGV2ZW50ID0+IHtcbiAgICAgICAgICAgIGNvbnN0ICR0YXJnZXQgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgLy8gdXBkYXRlIGNhcnQgcXVhbnRpdHlcbiAgICAgICAgICAgIGNhcnRVcGRhdGVRdHlUZXh0Q2hhbmdlKCR0YXJnZXQsIHByZVZhbCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICQoJy5jYXJ0LXJlbW92ZScsIHRoaXMuJGNhcnRDb250ZW50KS5vbignY2xpY2snLCBldmVudCA9PiB7XG4gICAgICAgICAgICBjb25zdCBpdGVtSWQgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmRhdGEoJ2NhcnRJdGVtaWQnKTtcbiAgICAgICAgICAgIGNvbnN0IHN0cmluZyA9ICQoZXZlbnQuY3VycmVudFRhcmdldCkuZGF0YSgnY29uZmlybURlbGV0ZScpO1xuICAgICAgICAgICAgc3dhbC5maXJlKHtcbiAgICAgICAgICAgICAgICB0ZXh0OiBzdHJpbmcsXG4gICAgICAgICAgICAgICAgaWNvbjogJ3dhcm5pbmcnLFxuICAgICAgICAgICAgICAgIHNob3dDYW5jZWxCdXR0b246IHRydWUsXG4gICAgICAgICAgICB9KS50aGVuKChyZXN1bHQpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAocmVzdWx0LnZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIHJlbW92ZSBpdGVtIGZyb20gY2FydFxuICAgICAgICAgICAgICAgICAgICBjYXJ0UmVtb3ZlSXRlbShpdGVtSWQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgJCgnW2RhdGEtaXRlbS1lZGl0XScsIHRoaXMuJGNhcnRDb250ZW50KS5vbignY2xpY2snLCBldmVudCA9PiB7XG4gICAgICAgICAgICBjb25zdCBpdGVtSWQgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmRhdGEoJ2l0ZW1FZGl0Jyk7XG5cbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAvLyBlZGl0IGl0ZW0gaW4gY2FydFxuICAgICAgICAgICAgdGhpcy5jYXJ0RWRpdE9wdGlvbnMoaXRlbUlkKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYmluZFByb21vQ29kZUV2ZW50cygpIHtcbiAgICAgICAgY29uc3QgJGNvdXBvbkNvbnRhaW5lciA9ICQoJy5jb3Vwb24tY29kZScpO1xuICAgICAgICBjb25zdCAkY291cG9uRm9ybSA9ICQoJy5jb3Vwb24tZm9ybScpO1xuICAgICAgICBjb25zdCAkY29kZUlucHV0ID0gJCgnW25hbWU9XCJjb3Vwb25jb2RlXCJdJywgJGNvdXBvbkZvcm0pO1xuXG4gICAgICAgICQoJy5jb3Vwb24tY29kZS1hZGQnKS5vbignY2xpY2snLCBldmVudCA9PiB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmhpZGUoKTtcbiAgICAgICAgICAgICRjb3Vwb25Db250YWluZXIuc2hvdygpO1xuICAgICAgICAgICAgJCgnLmNvdXBvbi1jb2RlLWNhbmNlbCcpLnNob3coKTtcbiAgICAgICAgICAgICRjb2RlSW5wdXQudHJpZ2dlcignZm9jdXMnKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgJCgnLmNvdXBvbi1jb2RlLWNhbmNlbCcpLm9uKCdjbGljaycsIGV2ZW50ID0+IHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICAgICRjb3Vwb25Db250YWluZXIuaGlkZSgpO1xuICAgICAgICAgICAgJCgnLmNvdXBvbi1jb2RlLWNhbmNlbCcpLmhpZGUoKTtcbiAgICAgICAgICAgICQoJy5jb3Vwb24tY29kZS1hZGQnKS5zaG93KCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICRjb3Vwb25Gb3JtLm9uKCdzdWJtaXQnLCBldmVudCA9PiB7XG4gICAgICAgICAgICBjb25zdCBjb2RlID0gJGNvZGVJbnB1dC52YWwoKTtcblxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgLy8gRW1wdHkgY29kZVxuICAgICAgICAgICAgaWYgKCFjb2RlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHN3YWwuZmlyZSh7XG4gICAgICAgICAgICAgICAgICAgIHRleHQ6ICRjb2RlSW5wdXQuZGF0YSgnZXJyb3InKSxcbiAgICAgICAgICAgICAgICAgICAgaWNvbjogJ2Vycm9yJyxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdXRpbHMuYXBpLmNhcnQuYXBwbHlDb2RlKGNvZGUsIChlcnIsIHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmRhdGEuc3RhdHVzID09PSAnc3VjY2VzcycpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWZyZXNoQ29udGVudCgpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHN3YWwuZmlyZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiByZXNwb25zZS5kYXRhLmVycm9ycy5qb2luKCdcXG4nKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb246ICdlcnJvcicsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBiaW5kR2lmdENlcnRpZmljYXRlRXZlbnRzKCkge1xuICAgICAgICBjb25zdCAkY2VydENvbnRhaW5lciA9ICQoJy5naWZ0LWNlcnRpZmljYXRlLWNvZGUnKTtcbiAgICAgICAgY29uc3QgJGNlcnRGb3JtID0gJCgnLmNhcnQtZ2lmdC1jZXJ0aWZpY2F0ZS1mb3JtJyk7XG4gICAgICAgIGNvbnN0ICRjZXJ0SW5wdXQgPSAkKCdbbmFtZT1cImNlcnRjb2RlXCJdJywgJGNlcnRGb3JtKTtcblxuICAgICAgICAkKCcuZ2lmdC1jZXJ0aWZpY2F0ZS1hZGQnKS5vbignY2xpY2snLCBldmVudCA9PiB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgJChldmVudC5jdXJyZW50VGFyZ2V0KS50b2dnbGUoKTtcbiAgICAgICAgICAgICRjZXJ0Q29udGFpbmVyLnRvZ2dsZSgpO1xuICAgICAgICAgICAgJCgnLmdpZnQtY2VydGlmaWNhdGUtY2FuY2VsJykudG9nZ2xlKCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICQoJy5naWZ0LWNlcnRpZmljYXRlLWNhbmNlbCcpLm9uKCdjbGljaycsIGV2ZW50ID0+IHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAkY2VydENvbnRhaW5lci50b2dnbGUoKTtcbiAgICAgICAgICAgICQoJy5naWZ0LWNlcnRpZmljYXRlLWFkZCcpLnRvZ2dsZSgpO1xuICAgICAgICAgICAgJCgnLmdpZnQtY2VydGlmaWNhdGUtY2FuY2VsJykudG9nZ2xlKCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICRjZXJ0Rm9ybS5vbignc3VibWl0JywgZXZlbnQgPT4ge1xuICAgICAgICAgICAgY29uc3QgY29kZSA9ICRjZXJ0SW5wdXQudmFsKCk7XG5cbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICAgIGlmICghZ2lmdENlcnRDaGVjayhjb2RlKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBzd2FsLmZpcmUoe1xuICAgICAgICAgICAgICAgICAgICB0ZXh0OiAkY2VydElucHV0LmRhdGEoJ2Vycm9yJyksXG4gICAgICAgICAgICAgICAgICAgIGljb246ICdlcnJvcicsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHV0aWxzLmFwaS5jYXJ0LmFwcGx5R2lmdENlcnRpZmljYXRlKGNvZGUsIChlcnIsIHJlc3ApID0+IHtcbiAgICAgICAgICAgICAgICBpZiAocmVzcC5kYXRhLnN0YXR1cyA9PT0gJ3N1Y2Nlc3MnKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVmcmVzaENvbnRlbnQoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBzd2FsLmZpcmUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogcmVzcC5kYXRhLmVycm9ycy5qb2luKCdcXG4nKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb246ICdlcnJvcicsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBiaW5kR2lmdFdyYXBwaW5nRXZlbnRzKCkge1xuICAgICAgICBjb25zdCBtb2RhbCA9IGRlZmF1bHRNb2RhbCgpO1xuXG4gICAgICAgICQoJ1tkYXRhLWl0ZW0tZ2lmdHdyYXBdJykub24oJ2NsaWNrJywgZXZlbnQgPT4ge1xuICAgICAgICAgICAgY29uc3QgaXRlbUlkID0gJChldmVudC5jdXJyZW50VGFyZ2V0KS5kYXRhKCdpdGVtR2lmdHdyYXAnKTtcbiAgICAgICAgICAgIGNvbnN0IG9wdGlvbnMgPSB7XG4gICAgICAgICAgICAgICAgdGVtcGxhdGU6ICdjYXJ0L21vZGFscy9naWZ0LXdyYXBwaW5nLWZvcm0nLFxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgbW9kYWwub3BlbigpO1xuXG4gICAgICAgICAgICB1dGlscy5hcGkuY2FydC5nZXRJdGVtR2lmdFdyYXBwaW5nT3B0aW9ucyhpdGVtSWQsIG9wdGlvbnMsIChlcnIsIHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgbW9kYWwudXBkYXRlQ29udGVudChyZXNwb25zZS5jb250ZW50KTtcblxuICAgICAgICAgICAgICAgIHRoaXMuYmluZEdpZnRXcmFwcGluZ0Zvcm0oKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBiaW5kR2lmdFdyYXBwaW5nRm9ybSgpIHtcbiAgICAgICAgJCgnLmdpZnRXcmFwcGluZy1zZWxlY3QnKS5vbignY2hhbmdlJywgZXZlbnQgPT4ge1xuICAgICAgICAgICAgY29uc3QgJHNlbGVjdCA9ICQoZXZlbnQuY3VycmVudFRhcmdldCk7XG4gICAgICAgICAgICBjb25zdCBpZCA9ICRzZWxlY3QudmFsKCk7XG4gICAgICAgICAgICBjb25zdCBpbmRleCA9ICRzZWxlY3QuZGF0YSgnaW5kZXgnKTtcblxuICAgICAgICAgICAgaWYgKCFpZCkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29uc3QgYWxsb3dNZXNzYWdlID0gJHNlbGVjdC5maW5kKGBvcHRpb25bdmFsdWU9JHtpZH1dYCkuZGF0YSgnYWxsb3dNZXNzYWdlJyk7XG5cbiAgICAgICAgICAgICQoYC5naWZ0V3JhcHBpbmctaW1hZ2UtJHtpbmRleH1gKS5oaWRlKCk7XG4gICAgICAgICAgICAkKGAjZ2lmdFdyYXBwaW5nLWltYWdlLSR7aW5kZXh9LSR7aWR9YCkuc2hvdygpO1xuXG4gICAgICAgICAgICBpZiAoYWxsb3dNZXNzYWdlKSB7XG4gICAgICAgICAgICAgICAgJChgI2dpZnRXcmFwcGluZy1tZXNzYWdlLSR7aW5kZXh9YCkuc2hvdygpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkKGAjZ2lmdFdyYXBwaW5nLW1lc3NhZ2UtJHtpbmRleH1gKS5oaWRlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgICQoJy5naWZ0V3JhcHBpbmctc2VsZWN0JykudHJpZ2dlcignY2hhbmdlJyk7XG5cbiAgICAgICAgZnVuY3Rpb24gdG9nZ2xlVmlld3MoKSB7XG4gICAgICAgICAgICBjb25zdCB2YWx1ZSA9ICQoJ2lucHV0OnJhZGlvW25hbWUgPVwiZ2lmdHdyYXB0eXBlXCJdOmNoZWNrZWQnKS52YWwoKTtcbiAgICAgICAgICAgIGNvbnN0ICRzaW5nbGVGb3JtID0gJCgnLmdpZnRXcmFwcGluZy1zaW5nbGUnKTtcbiAgICAgICAgICAgIGNvbnN0ICRtdWx0aUZvcm0gPSAkKCcuZ2lmdFdyYXBwaW5nLW11bHRpcGxlJyk7XG5cbiAgICAgICAgICAgIGlmICh2YWx1ZSA9PT0gJ3NhbWUnKSB7XG4gICAgICAgICAgICAgICAgJHNpbmdsZUZvcm0uc2hvdygpO1xuICAgICAgICAgICAgICAgICRtdWx0aUZvcm0uaGlkZSgpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkc2luZ2xlRm9ybS5oaWRlKCk7XG4gICAgICAgICAgICAgICAgJG11bHRpRm9ybS5zaG93KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAkKCdbbmFtZT1cImdpZnR3cmFwdHlwZVwiXScpLm9uKCdjbGljaycsIHRvZ2dsZVZpZXdzKTtcblxuICAgICAgICB0b2dnbGVWaWV3cygpO1xuICAgIH1cblxuICAgIGJpbmRFdmVudHMoKSB7XG4gICAgICAgIHRoaXMuYmluZENhcnRFdmVudHMoKTtcbiAgICAgICAgdGhpcy5iaW5kUHJvbW9Db2RlRXZlbnRzKCk7XG4gICAgICAgIHRoaXMuYmluZEdpZnRXcmFwcGluZ0V2ZW50cygpO1xuICAgICAgICB0aGlzLmJpbmRHaWZ0Q2VydGlmaWNhdGVFdmVudHMoKTtcblxuICAgICAgICAvLyBpbml0aWF0ZSBzaGlwcGluZyBlc3RpbWF0b3IgbW9kdWxlXG4gICAgICAgIHRoaXMuc2hpcHBpbmdFc3RpbWF0b3IgPSBuZXcgU2hpcHBpbmdFc3RpbWF0b3IoJCgnW2RhdGEtc2hpcHBpbmctZXN0aW1hdG9yXScpKTtcbiAgICB9XG59XG4iLCJpbXBvcnQgJCBmcm9tICdqcXVlcnknO1xuaW1wb3J0IHN0YXRlQ291bnRyeSBmcm9tICcuLi9jb21tb24vc3RhdGUtY291bnRyeSc7XG5pbXBvcnQgbm9kIGZyb20gJy4uL2NvbW1vbi9ub2QnO1xuaW1wb3J0IHV0aWxzIGZyb20gJ0BiaWdjb21tZXJjZS9zdGVuY2lsLXV0aWxzJztcbmltcG9ydCB7IFZhbGlkYXRvcnMgfSBmcm9tICcuLi9jb21tb24vZm9ybS11dGlscyc7XG5pbXBvcnQgc3dhbCBmcm9tICcuLi9nbG9iYWwvc3dlZXQtYWxlcnQnO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaGlwcGluZ0VzdGltYXRvciB7XG4gICAgY29uc3RydWN0b3IoJGVsZW1lbnQpIHtcbiAgICAgICAgdGhpcy4kZWxlbWVudCA9ICRlbGVtZW50O1xuXG4gICAgICAgIHRoaXMuJHN0YXRlID0gJCgnW2RhdGEtZmllbGQtdHlwZT1cIlN0YXRlXCJdJywgdGhpcy4kZWxlbWVudCk7XG4gICAgICAgIHRoaXMuaW5pdEZvcm1WYWxpZGF0aW9uKCk7XG4gICAgICAgIHRoaXMuYmluZFN0YXRlQ291bnRyeUNoYW5nZSgpO1xuICAgICAgICB0aGlzLmJpbmRFc3RpbWF0b3JFdmVudHMoKTtcbiAgICB9XG5cbiAgICBpbml0Rm9ybVZhbGlkYXRpb24oKSB7XG4gICAgICAgIHRoaXMuc2hpcHBpbmdFc3RpbWF0b3IgPSAnZm9ybVtkYXRhLXNoaXBwaW5nLWVzdGltYXRvcl0nO1xuICAgICAgICB0aGlzLnNoaXBwaW5nVmFsaWRhdG9yID0gbm9kKHtcbiAgICAgICAgICAgIHN1Ym1pdDogYCR7dGhpcy5zaGlwcGluZ0VzdGltYXRvcn0gLnNoaXBwaW5nLWVzdGltYXRlLXN1Ym1pdGAsXG4gICAgICAgIH0pO1xuXG4gICAgICAgICQoJy5zaGlwcGluZy1lc3RpbWF0ZS1zdWJtaXQnLCB0aGlzLiRlbGVtZW50KS5vbignY2xpY2snLCBldmVudCA9PiB7XG4gICAgICAgICAgICAvLyBXaGVuIHN3aXRjaGluZyBiZXR3ZWVuIGNvdW50cmllcywgdGhlIHN0YXRlL3JlZ2lvbiBpcyBkeW5hbWljXG4gICAgICAgICAgICAvLyBPbmx5IHBlcmZvcm0gYSBjaGVjayBmb3IgYWxsIGZpZWxkcyB3aGVuIGNvdW50cnkgaGFzIGEgdmFsdWVcbiAgICAgICAgICAgIC8vIE90aGVyd2lzZSBhcmVBbGwoJ3ZhbGlkJykgd2lsbCBjaGVjayBjb3VudHJ5IGZvciB2YWxpZGl0eVxuICAgICAgICAgICAgaWYgKCQoYCR7dGhpcy5zaGlwcGluZ0VzdGltYXRvcn0gc2VsZWN0W25hbWU9XCJzaGlwcGluZy1jb3VudHJ5XCJdYCkudmFsKCkpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNoaXBwaW5nVmFsaWRhdG9yLnBlcmZvcm1DaGVjaygpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5zaGlwcGluZ1ZhbGlkYXRvci5hcmVBbGwoJ3ZhbGlkJykpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuYmluZFZhbGlkYXRpb24oKTtcbiAgICAgICAgdGhpcy5iaW5kU3RhdGVWYWxpZGF0aW9uKCk7XG4gICAgICAgIHRoaXMuYmluZFVQU1JhdGVzKCk7XG4gICAgfVxuXG4gICAgYmluZFZhbGlkYXRpb24oKSB7XG4gICAgICAgIHRoaXMuc2hpcHBpbmdWYWxpZGF0b3IuYWRkKFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBzZWxlY3RvcjogYCR7dGhpcy5zaGlwcGluZ0VzdGltYXRvcn0gc2VsZWN0W25hbWU9XCJzaGlwcGluZy1jb3VudHJ5XCJdYCxcbiAgICAgICAgICAgICAgICB2YWxpZGF0ZTogKGNiLCB2YWwpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgY291bnRyeUlkID0gTnVtYmVyKHZhbCk7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IGNvdW50cnlJZCAhPT0gMCAmJiAhTnVtYmVyLmlzTmFOKGNvdW50cnlJZCk7XG5cbiAgICAgICAgICAgICAgICAgICAgY2IocmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGVycm9yTWVzc2FnZTogJ1RoZSBcXCdDb3VudHJ5XFwnIGZpZWxkIGNhbm5vdCBiZSBibGFuay4nLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgXSk7XG4gICAgfVxuXG4gICAgYmluZFN0YXRlVmFsaWRhdGlvbigpIHtcbiAgICAgICAgdGhpcy5zaGlwcGluZ1ZhbGlkYXRvci5hZGQoW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHNlbGVjdG9yOiAkKGAke3RoaXMuc2hpcHBpbmdFc3RpbWF0b3J9IHNlbGVjdFtuYW1lPVwic2hpcHBpbmctc3RhdGVcIl1gKSxcbiAgICAgICAgICAgICAgICB2YWxpZGF0ZTogKGNiKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGxldCByZXN1bHQ7XG5cbiAgICAgICAgICAgICAgICAgICAgY29uc3QgJGVsZSA9ICQoYCR7dGhpcy5zaGlwcGluZ0VzdGltYXRvcn0gc2VsZWN0W25hbWU9XCJzaGlwcGluZy1zdGF0ZVwiXWApO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmICgkZWxlLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZWxlVmFsID0gJGVsZS52YWwoKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gZWxlVmFsICYmIGVsZVZhbC5sZW5ndGggJiYgZWxlVmFsICE9PSAnU3RhdGUvcHJvdmluY2UnO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgY2IocmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGVycm9yTWVzc2FnZTogJ1RoZSBcXCdTdGF0ZS9Qcm92aW5jZVxcJyBmaWVsZCBjYW5ub3QgYmUgYmxhbmsuJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgIF0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFRvZ2dsZSBiZXR3ZWVuIGRlZmF1bHQgc2hpcHBpbmcgYW5kIHVwcyBzaGlwcGluZyByYXRlc1xuICAgICAqL1xuICAgIGJpbmRVUFNSYXRlcygpIHtcbiAgICAgICAgY29uc3QgVVBTUmF0ZVRvZ2dsZSA9ICcuZXN0aW1hdG9yLWZvcm0tdG9nZ2xlVVBTUmF0ZSc7XG5cbiAgICAgICAgJCgnYm9keScpLm9uKCdjbGljaycsIFVQU1JhdGVUb2dnbGUsIChldmVudCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgJGVzdGltYXRvckZvcm1VcHMgPSAkKCcuZXN0aW1hdG9yLWZvcm0tLXVwcycpO1xuICAgICAgICAgICAgY29uc3QgJGVzdGltYXRvckZvcm1EZWZhdWx0ID0gJCgnLmVzdGltYXRvci1mb3JtLS1kZWZhdWx0Jyk7XG5cbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICAgICRlc3RpbWF0b3JGb3JtVXBzLnRvZ2dsZUNsYXNzKCd1LWhpZGRlblZpc3VhbGx5Jyk7XG4gICAgICAgICAgICAkZXN0aW1hdG9yRm9ybURlZmF1bHQudG9nZ2xlQ2xhc3MoJ3UtaGlkZGVuVmlzdWFsbHknKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYmluZFN0YXRlQ291bnRyeUNoYW5nZSgpIHtcbiAgICAgICAgbGV0ICRsYXN0O1xuXG4gICAgICAgIC8vIFJlcXVlc3RzIHRoZSBzdGF0ZXMgZm9yIGEgY291bnRyeSB3aXRoIEFKQVhcbiAgICAgICAgc3RhdGVDb3VudHJ5KHRoaXMuJHN0YXRlLCB0aGlzLmNvbnRleHQsIHsgdXNlSWRGb3JTdGF0ZXM6IHRydWUgfSwgKGVyciwgZmllbGQpID0+IHtcbiAgICAgICAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgICAgICAgICBzd2FsLmZpcmUoe1xuICAgICAgICAgICAgICAgICAgICB0ZXh0OiBlcnIsXG4gICAgICAgICAgICAgICAgICAgIGljb246ICdlcnJvcicsXG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoZXJyKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29uc3QgJGZpZWxkID0gJChmaWVsZCk7XG5cbiAgICAgICAgICAgIGlmICh0aGlzLnNoaXBwaW5nVmFsaWRhdG9yLmdldFN0YXR1cyh0aGlzLiRzdGF0ZSkgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zaGlwcGluZ1ZhbGlkYXRvci5yZW1vdmUodGhpcy4kc3RhdGUpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoJGxhc3QpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNoaXBwaW5nVmFsaWRhdG9yLnJlbW92ZSgkbGFzdCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICgkZmllbGQuaXMoJ3NlbGVjdCcpKSB7XG4gICAgICAgICAgICAgICAgJGxhc3QgPSBmaWVsZDtcbiAgICAgICAgICAgICAgICB0aGlzLmJpbmRTdGF0ZVZhbGlkYXRpb24oKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJGZpZWxkLmF0dHIoJ3BsYWNlaG9sZGVyJywgJ1N0YXRlL3Byb3ZpbmNlJyk7XG4gICAgICAgICAgICAgICAgVmFsaWRhdG9ycy5jbGVhblVwU3RhdGVWYWxpZGF0aW9uKGZpZWxkKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gV2hlbiB5b3UgY2hhbmdlIGEgY291bnRyeSwgeW91IHN3YXAgdGhlIHN0YXRlL3Byb3ZpbmNlIGJldHdlZW4gYW4gaW5wdXQgYW5kIGEgc2VsZWN0IGRyb3Bkb3duXG4gICAgICAgICAgICAvLyBOb3QgYWxsIGNvdW50cmllcyByZXF1aXJlIHRoZSBwcm92aW5jZSB0byBiZSBmaWxsZWRcbiAgICAgICAgICAgIC8vIFdlIGhhdmUgdG8gcmVtb3ZlIHRoaXMgY2xhc3Mgd2hlbiB3ZSBzd2FwIHNpbmNlIG5vZCB2YWxpZGF0aW9uIGRvZXNuJ3QgY2xlYW51cCBmb3IgdXNcbiAgICAgICAgICAgICQodGhpcy5zaGlwcGluZ0VzdGltYXRvcikuZmluZCgnLmZvcm0tZmllbGQtLXN1Y2Nlc3MnKS5yZW1vdmVDbGFzcygnZm9ybS1maWVsZC0tc3VjY2VzcycpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBiaW5kRXN0aW1hdG9yRXZlbnRzKCkge1xuICAgICAgICBjb25zdCAkZXN0aW1hdG9yQ29udGFpbmVyID0gJCgnLnNoaXBwaW5nLWVzdGltYXRvcicpO1xuICAgICAgICBjb25zdCAkZXN0aW1hdG9yRm9ybSA9ICQoJy5lc3RpbWF0b3ItZm9ybScpO1xuXG4gICAgICAgICRlc3RpbWF0b3JGb3JtLm9uKCdzdWJtaXQnLCBldmVudCA9PiB7XG4gICAgICAgICAgICBjb25zdCBwYXJhbXMgPSB7XG4gICAgICAgICAgICAgICAgY291bnRyeV9pZDogJCgnW25hbWU9XCJzaGlwcGluZy1jb3VudHJ5XCJdJywgJGVzdGltYXRvckZvcm0pLnZhbCgpLFxuICAgICAgICAgICAgICAgIHN0YXRlX2lkOiAkKCdbbmFtZT1cInNoaXBwaW5nLXN0YXRlXCJdJywgJGVzdGltYXRvckZvcm0pLnZhbCgpLFxuICAgICAgICAgICAgICAgIGNpdHk6ICQoJ1tuYW1lPVwic2hpcHBpbmctY2l0eVwiXScsICRlc3RpbWF0b3JGb3JtKS52YWwoKSxcbiAgICAgICAgICAgICAgICB6aXBfY29kZTogJCgnW25hbWU9XCJzaGlwcGluZy16aXBcIl0nLCAkZXN0aW1hdG9yRm9ybSkudmFsKCksXG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICB1dGlscy5hcGkuY2FydC5nZXRTaGlwcGluZ1F1b3RlcyhwYXJhbXMsICdjYXJ0L3NoaXBwaW5nLXF1b3RlcycsIChlcnIsIHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgJCgnLnNoaXBwaW5nLXF1b3RlcycpLmh0bWwocmVzcG9uc2UuY29udGVudCk7XG5cbiAgICAgICAgICAgICAgICAvLyBiaW5kIHRoZSBzZWxlY3QgYnV0dG9uXG4gICAgICAgICAgICAgICAgJCgnLnNlbGVjdC1zaGlwcGluZy1xdW90ZScpLm9uKCdjbGljaycsIGNsaWNrRXZlbnQgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBxdW90ZUlkID0gJCgnLnNoaXBwaW5nLXF1b3RlOmNoZWNrZWQnKS52YWwoKTtcblxuICAgICAgICAgICAgICAgICAgICBjbGlja0V2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICAgICAgICAgICAgdXRpbHMuYXBpLmNhcnQuc3VibWl0U2hpcHBpbmdRdW90ZShxdW90ZUlkLCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24ucmVsb2FkKCk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICQoJy5zaGlwcGluZy1lc3RpbWF0ZS1zaG93Jykub24oJ2NsaWNrJywgZXZlbnQgPT4ge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgJChldmVudC5jdXJyZW50VGFyZ2V0KS5oaWRlKCk7XG4gICAgICAgICAgICAkZXN0aW1hdG9yQ29udGFpbmVyLnJlbW92ZUNsYXNzKCd1LWhpZGRlblZpc3VhbGx5Jyk7XG4gICAgICAgICAgICAkKCcuc2hpcHBpbmctZXN0aW1hdGUtaGlkZScpLnNob3coKTtcbiAgICAgICAgfSk7XG5cblxuICAgICAgICAkKCcuc2hpcHBpbmctZXN0aW1hdGUtaGlkZScpLm9uKCdjbGljaycsIGV2ZW50ID0+IHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICAgICRlc3RpbWF0b3JDb250YWluZXIuYWRkQ2xhc3MoJ3UtaGlkZGVuVmlzdWFsbHknKTtcbiAgICAgICAgICAgICQoJy5zaGlwcGluZy1lc3RpbWF0ZS1zaG93Jykuc2hvdygpO1xuICAgICAgICAgICAgJCgnLnNoaXBwaW5nLWVzdGltYXRlLWhpZGUnKS5oaWRlKCk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIChjZXJ0KSB7XG4gICAgaWYgKHR5cGVvZiBjZXJ0ICE9PSAnc3RyaW5nJykge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgLy8gQWRkIGFueSBjdXN0b20gZ2lmdCBjZXJ0aWZpY2F0ZSB2YWxpZGF0aW9uIGxvZ2ljIGhlcmVcbiAgICByZXR1cm4gdHJ1ZTtcbn1cbiJdLCJzb3VyY2VSb290IjoiIn0=