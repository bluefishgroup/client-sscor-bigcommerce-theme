import { hooks } from '@bigcommerce/stencil-utils';
import CatalogPage from './catalog';
import $ from 'jquery';
import compareProducts from './global/compare-products';
import FacetedSearch from './common/faceted-search';

export default class Category extends CatalogPage {
    onReady() {
        compareProducts(this.context.urls);

        if ($('#facetedSearch').length > 0) {
            this.initFacetedSearch();
        } else {
            this.onSortBySubmit = this.onSortBySubmit.bind(this);
            hooks.on('sortBy-submitted', this.onSortBySubmit);
        }

        this.sscorShippingHandler();
    }

    sscorShippingHandler() {

        var cookies = document.cookie;
        var customerId = $('#customer-id').val();
        var guestCookieExists = cookies.indexOf('guestuser') > 0;

        if (!guestCookieExists || (customerId == undefined && customerId == '')) {
            //show prices only to logged in customers
             console.log('sscorShippingHandler - NEED GUEST CHECK');
            var price =  $('.price-section .price').text().trim();
            if (price.startsWith('$')) {
                $('<div class="" style="display:block;margin-bottom:20px;"><a href="#" class="see-price btn" title="See Price">See Pricing Info</a></div>').insertAfter('.productView-info');
                $('.price-section .price.price--withoutTax').remove();
                $('.form-field--increments').remove();
                $('#form-action-addToCart').remove();
            } else {
                $('.price-section .price.price--withoutTax').show();
                $('.price-visibility .price-section').show();
                $('.price-visibility .add-to-cart-button').show();
                $('.form-field--increments').show();
                $('#form-action-addToCart').show();


            }

        } else {
            console.log('sscorShippingHandler - CUSTOMER');
            $('.price-section .price.price--withoutTax').show();
            $('.price-visibility .price-section').show();
            $('.price-visibility .add-to-cart-button').show();
            $('.form-field--increments').show();
            $('#form-action-addToCart').show();
        }


        $('.see-price').on('click', function(evt){
            evt.preventDefault();
            var url = document.querySelector("link[rel='canonical']").href;
            var name = $('.productView-title').text();
            document.cookie = "price_product="+ url +"; expires=Thu, 18 Dec 2099 12:00:00 UTC; path=/";
            document.cookie = "price_name="+ name +"; expires=Thu, 18 Dec 2099 12:00:00 UTC; path=/";
            window.location.href = ('/guest-check-in/');
        });

        $('embed').attr('width', '400px').attr('height','400px');


    }

    initFacetedSearch() {
        const $productListingContainer = $('#product-listing-container');
        const $facetedSearchContainer = $('#faceted-search-container');
        const productsPerPage = this.context.categoryProductsPerPage;
        const requestOptions = {
            config: {
                category: {
                    shop_by_price: true,
                    products: {
                        limit: productsPerPage,
                    },
                },
            },
            template: {
                productListing: 'category/product-listing',
                sidebar: 'category/sidebar',
            },
            showMore: 'category/show-more',
        };

        this.facetedSearch = new FacetedSearch(requestOptions, (content) => {
            $productListingContainer.html(content.productListing);
            $facetedSearchContainer.html(content.sidebar);

            $('body').triggerHandler('compareReset');

            $('html, body').animate({
                scrollTop: 0,
            }, 100);
        });
    }
}
