/*
 Import all product specific js
 */
import PageManager from './page-manager';
import Review from './product/reviews';
import collapsibleFactory from './common/collapsible';
import ProductDetails from './common/product-details';
import videoGallery from './product/video-gallery';
import { classifyForm } from './common/form-utils';
import $ from 'jquery';

export default class Product extends PageManager {

    constructor(context) {
        super(context);
        this.url = window.location.href;
        this.$reviewLink = $('[data-reveal-id="modal-review-form"]');
        this.$bulkPricingLink = $('[data-reveal-id="modal-bulk-pricing"]');
    }

    onReady() {
        // Listen for foundation modal close events to sanitize URL after review.
        $(document).on('close.fndtn.reveal', () => {
            if (this.url.indexOf('#write_review') !== -1 && typeof window.history.replaceState === 'function') {
                window.history.replaceState(null, document.title, window.location.pathname);
            }
        });

        let validator;

        // Init collapsible
        collapsibleFactory();

        this.productDetails = new ProductDetails($('.productView'), this.context, window.BCData.product_attributes);
        this.productDetails.setProductVariant();

        videoGallery();

        const $reviewForm = classifyForm('.writeReview-form');
        const review = new Review($reviewForm);

        $('body').on('click', '[data-reveal-id="modal-review-form"]', () => {
            validator = review.registerValidation(this.context);
        });

        $reviewForm.on('submit', () => {
            if (validator) {
                validator.performCheck();
                return validator.areAll('valid');
            }

            return false;
        });

        this.productReviewHandler();
        this.bulkPricingHandler();
        this.sscorShippingHandler();
    }

    sscorShippingHandler() {

        var cookies = document.cookie;
        var customerId = $('#customer-id').val();
        var guestCookieExists = cookies.indexOf('guestuser') > 0;

        if (!guestCookieExists || (customerId == undefined && customerId == '')) {
            //show prices only to logged in customers
          //  console.log('sscorShippingHandler - NEED GUEST CHECK');
                var price =  $('.price-section .price').text().trim();
                if (price.startsWith('$')) {
                    $('<div class="" style="display:block;margin-bottom:20px;"><a href="#" class="see-price btn" title="See Price">See Pricing Info</a></div>').insertAfter('.productView-info');
                    $('.price-section .price.price--withoutTax').remove();
                    $('.form-field--increments').remove();
                    $('#form-action-addToCart').remove  ();
                } else {
                    $('.price-section .price.price--withoutTax').show();
                    $('.price-visibility .price-section').show();
                    $('.price-visibility .add-to-cart-button').show();
                    $('.form-field--increments').show();
                    $('#form-action-addToCart').show();
                }

            } else {
           //     console.log('sscorShippingHandler - CUSTOMER');
            $('.price-section .price.price--withoutTax').show();
            $('.price-visibility .price-section').show();
            $('.price-visibility .add-to-cart-button').show();
            $('.form-field--increments').show();
            $('#form-action-addToCart').show();
            }


            $('.see-price').on('click', function(evt){
                evt.preventDefault();
                var url = document.querySelector("link[rel='canonical']").href;
                var name = $('.productView-title').text();
                document.cookie = "price_product="+ url +"; expires=Thu, 18 Dec 2099 12:00:00 UTC; path=/";
                document.cookie = "price_name="+ name +"; expires=Thu, 18 Dec 2099 12:00:00 UTC; path=/";
                window.location.href = ('/guest-check-in/');
            });

            $('embed').attr('width', '400px').attr('height','400px');


    }

    productReviewHandler() {
        if (this.url.indexOf('#write_review') !== -1) {
            this.$reviewLink.trigger('click');
        }
    }

    bulkPricingHandler() {
        if (this.url.indexOf('#bulk_pricing') !== -1) {
            this.$bulkPricingLink.trigger('click');
        }
    }
}
