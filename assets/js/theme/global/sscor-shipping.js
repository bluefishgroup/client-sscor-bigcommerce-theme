import $ from 'jquery';


export default function (context) {


    Number.isInteger = Number.isInteger || function(value) {
        return typeof value === "number" &&
            isFinite(value) &&
            Math.floor(value) === value;
    };

    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function(searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        };
    }

    function setShippingMessage(inv, quantity, isDefault) {
        var leadtime = 5;
        var trigger = 25;
        var nbdMsg = " Scheduled to ship next business day";
        var leadtimeMsg = " Scheduled to ship in " + (1 + leadtime) + " business days";
        var available = inv;

        var qty = quantity;
        var diff = available - qty;
        var message = '';

        if (isDefault) {
            //quantity is 1 by default
            if (available > 0 && quantity <= available ) {
                message = 'Ships Next Business Day';
            } else if (available <= 0 && diff > -25) {
                message = 'Ships in '  + (1 + leadtime) + ' business days';
            } else if (available <= 0 && diff <= -25) {
                message = 'Please contact us for a ship date';
            }

        } else {

            if (diff >= 0 ) {
                if (diff < available && available > 0) {
                    message = qty + nbdMsg;
                }

            } else  {
                if (diff < trigger  && available > 0 ) {
                    if (diff <= -trigger) {
                        message = 'Please contact us for a ship date';
                    } else {
                        message = available + nbdMsg +"; "+ -diff + leadtimeMsg;
                    }

                } else if (-diff < trigger  && available <= 0 )  {
                    message = leadtimeMsg;

                } else {
                    message = 'Please contact us for a ship date';

                }
            }

        }

        //console.log( "  Delta is:  " + diff + " , Qty Requested: " + quantity +" , Qty On Hand: " + available);

        return message;

    }


    //product detail page
    var inventory = parseInt($('#sscor-inventory').text());
    var msg = '';
    if (Number.isInteger(inventory)) {
        msg = setShippingMessage(inventory, 1, true);
        $('#sscor-availability').text(msg);
    } else {
        $('#sscor-availability').parent().hide();
    }

}