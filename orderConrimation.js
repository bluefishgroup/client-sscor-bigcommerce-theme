<script>
    (function(win) {
    'use strict';

    var listeners = [],
    doc = win.document,
    MutationObserver = win.MutationObserver || win.WebKitMutationObserver,
    observer;

    function ready(selector, fn) {
    // Store the selector and callback to be monitored
    listeners.push({
    selector: selector,
    fn: fn
});
    if (!observer) {
    // Watch for changes in the document
    observer = new MutationObserver(check);
    observer.observe(doc.documentElement, {
    childList: true,
    subtree: true
});
}
    // Check if the element is currently in the DOM
    check();
}

    function check() {
    // Check the DOM for elements matching a stored selector
    for (var i = 0, len = listeners.length, listener, elements; i < len; i++) {
    listener = listeners[i];
    // Query for elements matching the specified selector
    elements = doc.querySelectorAll(listener.selector);
    for (var j = 0, jLen = elements.length, element; j < jLen; j++) {
    element = elements[j];
    // Make sure the callback isn't invoked with the
    // same element more than once
    if (!element.ready) {
    element.ready = true;
    // Invoke the callback with the element
    listener.fn.call(element, element);
}
}
}
}
    // Expose `ready`
    win.ready = ready;

})(this);

    ready('.continueButtonContainer', function(element) {

        var div = document.createElement("div");
        div.id = 'hubspot-form';
        div.className = 'hubspot-form';
        div.style = 'margin: 0 auto;text-align:center';
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.innerHTML = "hbspt.forms.create({" +
        "            portalId: \"212347\",\n" +
        "            formId: \"a4f331aa-aaf1-4e8b-bf70-bdc576af2a72\"\n" +
        "        });";
        s.text = "hbspt.forms.create({" +
        "            portalId: \"212347\",\n" +
        "            formId: \"a4f331aa-aaf1-4e8b-bf70-bdc576af2a72\"\n" +
        "        });";
        div.appendChild(s);
        console.log(div);
        console.log(s);

        var container = document.getElementsByClassName('continueButtonContainer');
     container[0].insertAdjacentElement("beforebegin", div);
});
</script>