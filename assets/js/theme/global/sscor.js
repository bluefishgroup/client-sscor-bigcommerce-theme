import $ from 'jquery';

export default function (context) {

    $("h1:contains('S-SCORT'), h3:contains('S-SCORT'), a:contains('S-SCORT'), p:contains('S-SCORT')").each(function () {

        $(this).html($(this).html().replace("S-SCORT", "<span style='color:#e74c3c'>S</span>-SCORT"));
    });

    $("h1:contains('SSCOR'), h3:contains('SSCOR'), a:contains('SSCOR'), p:contains('SSCOR')").each(function () {

        $(this).html($(this).html().replace("SSCOR", "<span style='color:#e74c3c'>S</span>SCOR"));
    });

    var customerId = $('#customer-id').val();
    var cookies = document.cookie;
    var guestCookieExists = cookies.indexOf('guest_status') > 0;

    if (guestCookieExists) {
        console.log('Verified Guest by Cookie');
        $('.price-section .price.price--withoutTax').show();
        $('.price-visibility .price-section').show();
        $('.price-visibility .add-to-cart-button').show();
        $('.form-field--increments').show();
        $('#form-action-addToCart').show();
    } else if (customerId != undefined && customerId != '') {
        console.log('Verified Guest by Login');
        // document.cookie = "cookiename=guest_status ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
        document.cookie = "guest_status=guestuser; expires=Thu, 18 Dec 2099 12:00:00 UTC; path=/";
        //  $.cookie('guest_status', 'guestuser',  { expires: 9999, domain: 'sscor.com', path: '/' });
        $('.price-section .price.price--withoutTax').show();
        $('.price-visibility .price-section').show();
        $('.price-visibility .add-to-cart-button').show();
        $('.form-field--increments').show();
        $('#form-action-addToCart').show();
    } else  {
        console.log("Don't show prices until guest is checked in!");
    }
}
